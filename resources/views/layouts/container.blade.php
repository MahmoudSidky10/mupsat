<!DOCTYPE html>
<html dir="rtl">
@include('includes.site.head')
@yield('css')
<body>
<div class="content">
    @yield('content')
</div>
@include('includes.site.footer')
@include('includes.site.scripts')
@yield('js')
</body>
</html>