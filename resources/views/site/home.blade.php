
<!DOCTYPE html>
<html>

<!-- Mirrored from www.demo.picalica.net/xphone/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Mar 2018 17:37:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>تطبيق مبسط لمواد البناء</title>
    <!--===================================-->
    <!--====== CSS Libs ===================-->
    <!--===================================-->
    <link rel="stylesheet" href="{{asset('front_assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front_assets/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('front_assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('front_assets/css/scss/style.css')}}">
    <!--====== Fonts ======================-->
    <!--===================================-->
    <link rel="stylesheet" href="../../fonts.googleapis.com/earlyaccess/notokufiarabic.css">
    <link rel="stylesheet" href="../../fonts.googleapis.com/earlyaccess/droidarabicnaskh.css">





    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" >

<div class="container-fluid">
    <div class="row">
        <div class="sidebar">
            <div class="brand"><img src="{{asset('front_assets/img/mp.jpg')}}" alt=""></div>
            <ul class="nav nav-tabs nav-stacked">
                <li class="active"><a href="#1">الرئيسية</a></li>
                <li><a href='#2'>لقطات</a></li>
                <li><a href='#3'>ميزات تطبيقنا</a></li>
                <li><a href='#5'>حمل التطبيق</a></li>
                <li><a href="#6">أتصل بنا</a></li>
            </ul>
            <ul class="social-links text-center">
                <li><a href="#!"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#!"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#!"><i class="fa fa-youtube"></i></a></li>
                <li><a href="#!"><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
        <div class="main-container">
            <div class="img-device">
                <div class="background-device">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="{{asset('front_assets/img/02.jpg')}}" alt="01">
                            </div>
                            <div class="item">
                                <img src="{{asset('front_assets/img/03.jpg')}}" alt="02">
                            </div>
                            <div class="item">
                                <img src="{{asset('front_assets/img/01.jpg')}}" alt="03">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src="{{asset('front_assets/img/iphone-mockup.png')}}" alt="" class="device">
            <!-- Header -->
            <header class="jumbotron valign-wrapper" id="1">
                <div class="valign">
                    <h1>نبذة عن تطبيق مبسط لمود البناء</h1>
                    <p>تطبيق مبسط لبيع مواد البناء الأولية ذات الجودة العالية ، يقدم أسعار لن تجد أقل منها في السوق ، الأسعار تشمل الضريبة المضافة والشحن والتوصيل ، والتطبيق يخدم الجميع.

                    </p>
                    <a href="#!" class="btn btn-appstore"><img src="{{asset('front_assets/img/icon01.png')}}" alt="" width="110"></a>
                    <a href="#!" class="btn btn-playstore"><img src="{{asset('front_assets/img/icon02.png')}}" alt="" width="120"></a>
                </div>
            </header>

            <!-- Features section -->
            <section class="first valign-wrapper" id="2">
                <div class="detail col-xs-12">
                    <h3>لقطات من التطبيق</h3>
                    <p>يمكنكم عملائنا الكرام مشاهدت لقطات من التطبيق والصفحات الخاصة لتوضيح سهولة العمل عليه دون  اى صعوبات فى الاستخدام .</p>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/01.jpg')}}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/02.jpg')}}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/03.jpg')}}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/04.jpg')}}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/05.jpg')}}" alt="" class="img-responsive">
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <img src="{{asset('front_assets/img/06.jpg')}}" alt="" class="img-responsive">
                    </div>
                </div>
            </section>
            <section class="valign-wrapper bg-gray" id="3">
                <div class="valign">
                    <div class="section-title">
                        <h2>ميزات تطبيق مبسط</h2>
                        <p>من المميزات التى يقدمها تطبيق مبسط لبيع مواد البناء،</p>
                    </div>
                    <div class="featurs-wrapper col-xs-12">
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-shopping-bag"></i>
                                <div class="feature-info">
                                    <h4>شراء مباشر من التطبيق</h4>
                                    <p>يوفرتطبيق مبسط العديد من وسائل الشراء والدفع لتسهيل عميلة البيع من خلال التطبيق والعميل بكل يسر .</p>
                                </div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-lock"></i>
                                <div class="feature-info">
                                    <h4>توثيق آمن للحساب</h4>
                                    <p>من مميزات تطبيق مبسط الامان والحماية من خلال فريق الدعم لدية لحماية جميع معلوماتك وبياناتك الشخصية .</p>
                                </div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-shuttle-van"></i>
                                <div class="feature-info">
                                    <h4>سرعة في التحميل</h4>
                                    <p>تم تصميم وبرمجة تطبيق مبسط باحدث التقنيات الحديثة التى تسهل على العميل سرعة الاستخدام .</p>
                                </div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-mobile-alt"></i>
                                <div class="feature-info">
                                    <h4>سهولة الإستخدام</h4>
                                    <p>يمكنكم التعامل بكل سهولة  باستخدام تطبيق مبسط دون ايجاد اى صعوبات فى الاستخدام .</p>
                                </div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-comment-o"></i>
                                <div class="feature-info">
                                    <h4>سرعة المراسلة</h4>
                                    <p>يمكنكم المراسلة من خلال التطبيق بطريقة سهلة توفر السرعة الكاملة فى ارسال واستقبال الرسائل الخاصة .</p>
                                </div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="feature-desc">
                                <i class="icon fa fa-battery-full"></i>
                                <div class="feature-info">
                                    <h4>لا يستهلك البطارية</h4>
                                    <p>يمكنكم استخدام تطبيق مبسط لمود البناء دون الخوف من استهلاك البطارية حيث انة مثالى لاستخدام الطاقة .</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section class="download-app" id="5">
                <div class="detail col-xs-12">
                    <h3>لماذا اخترت تطبيق مبسط؟</h3>
                    <p>لا تصعب على نفسك عملية بناء بيتك أكثر من اللازم ؟..</p>
                    <p>مبسط البناء صار معاه أبسط ،سواء كنت تبني بيتك بنفسك أو كنت مقاول وتبني لعميل ،يوفر لك تطبيق مبسط مواد البناء الأولية ، من طوب وبلك وأسمنت وخرسانة جاهزة وحديد وأنواع الرمل والبحص اللي تحتاجه في بناء العظم ،أما الأسعار فهي من الآخر، سعر لن تجد أقل منه في السوق ،توصيل وشحن مجاني ،الإستعانة بالخبراء من أجل تحديث دوري ودقيق للأسعار،شراء مباشر من التطبيق ،..</p>
                    <p>وسرعة مراسلة ودقة في المواعيد ،يخدم جميع مناطق مدينة الرياض ،عند تنزيل الطلبيه لموقع المشروع حيكون معاك عضو من فريق مبسط حتى يتأكد إن كل قطعة وصلت لك سليمة من بعد عملية الشحن والتوصيل ،لدينا فريق عمل ضخم هدفه يوفر نقودك ويوفر لك المواد اللي تحتاجها من المصنع اللي تختاره ،هدف مبسط إنه يوفر نقودك ووقتك ويعطيك أفضل النتائج ..</p>
                    <a href="#!" class="btn-appstore"><img src="{{asset('front_assets/img/appstore-icon.png')}}" alt=""></a>
                    <a href="#!" class="btn-playstore"><img src="{{asset('front_assets/img/playstore-icon.png')}}" alt=""></a>
                </div>
            </section>
            <section class="" id="6">
                <div class="valign">
                    <div class="section-title">
                        <h2>تواصل معنا</h2>
                        <p>يمكنكم التواصل مع ادارة التطبيق من خلال النموذج التالى :</p>
                        <form id="contact-form" role="form" method="post" action="{{url('/sitecontact')}}">
                            <div class="messages"></div>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_name" required="required" type="text" name="name" class="form-control" placeholder="" onblur="if (this.value == '') {this.value = 'الأسم';}" onfocus="if (this.value == 'الأسم') {this.value = '';}" value="الأسم">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_lastname" required="required"   type="text" name="surname" class="form-control"  placeholder="" onblur="if (this.value == '') {this.value = 'الكنية';}" onfocus="if (this.value == 'الكنية') {this.value = '';}" value="الكنية">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_email" required="required"  type="email" name="email" class="form-control"  placeholder="" onblur="if (this.value == '') {this.value = 'البريد الإلكتروني';}" onfocus="if (this.value == 'البريد الإلكتروني') {this.value = '';}" value="البريد الإلكتروني">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_phone" required="required"  type="tel" name="number" class="form-control" placeholder="" onblur="if (this.value == '') {this.value = 'رقم الهاتف';}" onfocus="if (this.value == 'رقم الهاتف') {this.value = '';}" value="رقم الهاتف">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="form_title" required="required"  type="text" name="title" class="form-control" placeholder="عنوان الرساله" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="message" class="form-control"  placeholder="الرسالة"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-primary" value="أرسلها لنا">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!--===================================-->
<!--====== JS Libs ====================-->
<!--===================================-->

{{ Html::script('front_assets/js/jquery.min.js')}}
{{ Html::script('front_assets/js/bootstrap.min.js')}}

</body>

<!-- Mirrored from www.demo.picalica.net/xphone/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Mar 2018 17:37:53 GMT -->
</html>

