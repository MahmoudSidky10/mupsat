

<!DOCTYPE html>
<html dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Maher </title>
    <style>
        /*!
 * Bootstrap Grid v4.1.3 (https://getbootstrap.com/)
 * Copyright 2011-2018 The Bootstrap Authors
 * Copyright 2011-2018 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
        html {
            box-sizing: border-box;
            -ms-overflow-style: scrollbar; }

        *,
        *::before,
        *::after {
            box-sizing: inherit; }

        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto; }

        @media (min-width: 576px) {
            .container {
                max-width: 540px; } }
        @media (min-width: 768px) {
            .container {
                max-width: 720px; } }
        @media (min-width: 992px) {
            .container {
                max-width: 960px; } }
        @media (min-width: 1200px) {
            .container {
                max-width: 1140px; } }
        .container-fluid {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto; }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px; }

        .no-gutters {
            margin-right: 0;
            margin-left: 0; }

        .no-gutters > .col,
        .no-gutters > [class*="col-"] {
            padding-right: 0;
            padding-left: 0; }

        .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
        .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
        .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
        .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
        .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
        .col-xl-auto {
            position: relative;
            width: 100%;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px; }

        .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%; }

        .col-auto {
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: none; }

        .col-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%; }

        .col-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%; }

        .col-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%; }

        .col-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%; }

        .col-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%; }

        .col-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%; }

        .col-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%; }

        .col-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%; }

        .col-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%; }

        .col-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%; }

        .col-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%; }

        .col-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%; }

        * {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', sans-serif;
            color: gray; }

        html {
            height: 100%; }

        body {
            min-height: 100%;
            position: relative;
            font-size: 20px; }

        header {
            text-align: right;
            box-shadow: 0px 3px 10px 0px #262425; }
        header .logo, header .logo-letter {
            overflow: hidden;
            margin: auto;
            margin-bottom: 15px; }
        header .logo img, header .logo-letter img {
            max-width: 100%; }
        header .logo {
            text-align: right; }
        header .logo-letter {
            margin-top: 10px;
            text-align: left; }

        .content {
            margin-top: 30px; }
        .content h3 {
            text-transform: capitalize;
            font-weight: lighter;
            color: gray;
            margin: 50px 0; }
        @media (max-width: 768px) {
            .content h3 {
                margin: 10px 0; } }
        .content a {
            display: block;
            color: #91dc5a;
            text-transform: capitalize;
            text-decoration: none;
            margin-top: 50px;
            font-weight: bold; }
        .content P {
            line-height: 1.5; }
        .content P:last-of-type {
            margin: 20px 0; }
        .content P:last-of-type a {
            display: inline; }

        footer {
            background-color: #262425;
            padding: 40px 0;
            position: absolute;
            width: 100%;
            bottom: -102.5px;
            left: 0;
            right: 0; }
        @media (max-width: 768px) {
            footer {
                padding: 20px 0; } }
        footer .up {
            padding-top: 20px;
            padding-bottom: 20px;
            border-bottom: 1px solid #a8a2a5; }
        footer .up h3 {
            text-transform: capitalize;
            font-weight: lighter;
            color: #91dc5a; }
        @media (max-width: 768px) {
            footer .up h3 {
                text-align: center;
                margin: 30px 0; } }
        footer .up ul {
            display: flex;
            list-style: none;
            justify-content: flex-end; }
        @media (max-width: 768px) {
            footer .up ul {
                justify-content: space-between; } }
        footer .up ul li {
            margin-left: 25px; }
        @media (max-width: 768px) {
            footer .up ul li {
                margin-left: 0; }
            footer .up ul li a {
                display: block;
                padding: 0 5px;
                overflow: hidden; }
            footer .up ul li a img {
                max-width: 100%;
                max-height: 100%; } }

        /*# sourceMappingURL=main.css.map */

    </style>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="logo-letter col-6">
                <img src="images/Logo.png" alt="logo">
            </div>
            <div class="logo col-6">
                <img src="images/head.png" alt="logo-head">
            </div>
        </div>
    </div>
</header>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3> Hello {{ $user->name }}</h3>
                <p>we recevied a request to verfiy your email , Click the link below to di this :</p>
                <a href="{{ url("verifyEmail/$user->id") }}"> Verfiy Your Account</a>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="up col-12">
                <div class="row">
                    <div class="col-sm-6 col-12">
                        <h3>maher</h3>
                    </div>
                    <div class="col-sm-6 col-12">
                        <ul>
                            <li><a href="#"><img src="images/facebook.png" alt="facebook"></a></li>
                            <li><a href="#"><img src="images/twitter.png" alt="twitter"></a></li>
                            <li><a href="#"><img src="images/instagram.png" alt="instagram"></a></li>
                            <li><a href="#"><img src="images/linkedin.png" alt="linked-in"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
</body>
</html>