@php


    $lang = LaravelLocalization::getCurrentLocale();

         App::setLocale($lang);
@endphp


@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')

    <!-- BEGIN CONTENT BODY -->

    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px; margin-right: 20px;color: #fff" class="page-title">
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal"
                           href="#Addresponsive">{{ trans('language.add') }} </a>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>


                            <th class="all"> #</th>
                            <th class="all"> {{ trans('language.image') }}   </th>
                            <th class="all">  {{ trans('language.name_ar') }} </th>
                            <th class="all">  {{ trans('language.name_en') }} </th>
                            <th class="all">  {{ trans('language.unit_ar') }} </th>
                            <th class="all">  {{ trans('language.unit_en') }} </th>
                            <th class="all">  {{ trans('language.size_unit_ar') }} </th>
                            <th class="all">  {{ trans('language.size_unit_en') }} </th>
                            <th class="all"> {{ trans('language.earnest') }} </th>
                            <th class="all"> {{ trans('language.priceUnit_ar') }} </th>
                            <th class="all"> {{ trans('language.priceUnit_en') }} </th>
                            <th class="all"> {{ trans('language.typeSizeTitle_ar') }} </th>
                            <th class="all"> {{ trans('language.typeSizeTitle_en') }} </th>


                            <th class="all">  {{ trans('language.settings') }} </th>

                        </tr>


                        </thead>
                        <tbody>

                        @foreach($sections as $section)
                            @php
                                $name_ar  = " $section->name_ar ";
                            @endphp
                            
                            <div id="Editresponsive{{$section->id}}" class="modal fade" tabindex="-1" data-width="800" data-height="760">
                                {!! Form::open( array('route' => array('sections.update',$section->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'editform'))  !!}
                                {!! method_field('put') !!}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h4 class="modal-title"> {{ trans('language.edit') }}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group form-md-line-input">
                                                <div class="col-xs-12 label">
                                                    <label for="image">{{ trans('language.image') }}</label>
                                                </div>

                                                <div class="input">
                                                    <img id="upload" class="upload" width="100" height="100"

                                                         @if($section->image)

                                                         src="{{$section->image}}"

                                                         @else

                                                         src="{{ asset('AdminPanelAssetFiles/section.png') }}"

                                                        @endif

                                                    >
                                                </div>

                                                <input class="image" name="image" type="file">


                                            </div>

                                        </div>

                                        <p> {{ trans('language.name_ar') }}  </p>
                                        <input name="name_ar" value="{{ $section->name_ar }}" class="form-control"
                                               type="text">
                                        <p> {{ trans('language.name_en') }} </p>
                                        <input name="name_en" value="{{ $section->name_en }}" class="form-control"
                                               type="text">

                                        <p> {{ trans('language.unit_ar') }}  </p>
                                        <input name="unit_ar" value="{{ $section->unit_ar }}" class="form-control"
                                               type="text">
                                        <p> {{ trans('language.unit_en') }} </p>
                                        <input name="unit_en" value="{{ $section->unit_en }}" class="form-control"
                                               type="text">
                                        <p> {{ trans('language.size_unit_ar') }}  </p>
                                        <input name="size_unit_ar" value="{{ $section->size_unit_ar }}" class="form-control"
                                               type="text">
                                        <p> {{ trans('language.size_unit_en') }} </p>
                                        <input name="size_unit_en" value="{{ $section->size_unit_en }}" class="form-control"
                                               type="text">

                                               
                                               <p> {{ trans('language.priceUnit_ar') }}  </p>
                                                <input name="priceUnit_ar"
                                                       class="form-control" value="{{ $section->priceUnit_ar }}" type="text">

                                                <p> {{ trans('language.priceUnit_en') }}  </p>
                                                <input name="priceUnit_en"
                                                       class="form-control " value="{{ $section->priceUnit_en }}" type="text">



                                               <p> {{ trans('language.typeSizeTitle_ar') }}  </p>
                                                <input name="typeSizeTitle_ar"
                                                       class="form-control" value="{{ $section->typeSizeTitle_ar }}" type="text">

                                                <p> {{ trans('language.typeSizeTitle_en') }}  </p>
                                                <input name="typeSizeTitle_en"
                                                       class="form-control " value="{{ $section->typeSizeTitle_en }}" type="text">



                                        <p> {{ trans('language.earnest') }}  </p>
                                        <input name="earnest" value="{{ $section->earnest }}"
                                               class="form-control " type="text">


                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn green"> {{ trans('language.edit') }}</button>
                                    <button type="button" data-dismiss="modal"
                                            class="btn btn-outline dark"> {{ trans('language.close') }}</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>


                                    <img width="50px" height="50px" data-toggle="modal"
                                         data-target=".image_modal"
                                         class="view_image img-circle"
                                         @if($section->image)

                                         src="{{$section->image}}"

                                         @else

                                         src="{{ asset('AdminPanelAssetFiles/section.png') }}">

                                    @endif

                                </td>
                                <td>{{ $section->name_ar }}</td>
                                <td>{{ $section->name_en }}</td>
                                <td>{{ $section->unit_ar }}</td>
                                <td>{{ $section->unit_en }}</td>
                                <td>{{ $section->size_unit_ar }}</td>
                                <td>{{ $section->size_unit_en }}</td>
                                <td>{{ $section->earnest }}%</td>
                                <td class="centerCollom">{{ $section->priceUnit_ar }}</td>
                                    <td class="centerCollom">{{ $section->priceUnit_en }}</td>
                                    <td class="centerCollom">{{ $section->typeSizeTitle_ar }}</td>
                                    <td class="centerCollom">{{ $section->typeSizeTitle_en }}</td>


                                {{--  export as  --}}
                                <td>

                                    <ul class="" style="list-style: none">


                                        <li style="display: inline">
                                            {{-- responsive{{$region->id}} --}}
                                            <a title="تعديل بيانات المستخدم" class="btn btn-success" data-toggle="modal"
                                               href="#Editresponsive{{$section->id}}">
                                                <i class="fa fa-edit"></i> {{ trans('language.edit') }}  </a>
                                        </li>


                                        <li style="display: inline">
                                            {{--  DELETE BTN BTN   --}}
                                            <button data-id="" data-toggle="modal"
                                                    data-target="#delete_modal{{ $section->id }}"
                                                    class="btn btn-danger">
                                                <i class="fa fa-remove"></i>
                                                {{  trans('language.delete') }}
                                            </button>

                                            {{--  DELETE Form    --}}
                                            <div id="delete_modal{{ $section->id }}"
                                                 class="modal fade delete_modal bgmodal">
                                                <div class="modal-dialog modal-confirm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="icon-box">
                                                                <i class="material-icons">&#xE5CD;</i>
                                                            </div>
                                                            <h4 class="modal-title">{{ trans('language.delete_danger') }}</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">&times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p> {{ trans('language.delete_aleart') }} ( <span
                                                                    style="color: #f15e5e">  {{$section->name}} </span>
                                                                ) ؟ </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::open(['route'=>["sections.destroy" , $section->id ], 'class'=>"" ]) !!}
                                                            {!! method_field('DELETE') !!}
                                                            <button type="button" class="btn btn-info"
                                                                    data-dismiss="modal">{{ trans('language.close') }}</button>


                                                            <button type="submit"
                                                                    class="btn btn-danger">{{ trans('language.delete') }}</button>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <!-- responsive -->
        <div id="Addresponsive" class="modal fade" tabindex="-1" data-width="760" data-height="760">
            {!! Form::open( array('route' => array('sections.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'AddForm'))  !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4> {{ trans('language.add') }} {{ trans('language.sections') }}</h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-md-line-input">
                            <div class="col-xs-12 label">
                                <label for="image">{{__('language.image ')}}</label>
                            </div>

                            <div class="input">
                                <img id="upload" src="{{ asset('AdminPanelAssetFiles/section.png') }}" class="upload"
                                     width="100" height="100">

                            </div>

                            <input required="required" class="image" name="image" type="file">
                        </div>


                        <p> {{ trans('language.name_ar') }}  </p>
                        <input name="name_ar" value="" class="form-control" type="text">
                        <p>{{ trans('language.name_en') }}</p>
                        <input name="name_en" value="" class="form-control" type="text">

                        <p> {{ trans('language.unit_ar') }}  </p>
                        <input name="unit_ar" value="" class="form-control" type="text">
                        <p>{{ trans('language.unit_en') }}</p>
                        <input name="unit_en" value="" class="form-control" type="text">
                        <p> {{ trans('language.size_unit_ar') }}  </p>
                        <input name="size_unit_ar" value="" class="form-control" type="text">
                        <p>{{ trans('language.size_unit_en') }}</p>
                        <input name="size_unit_en" value="" class="form-control" type="text">


                    <p> {{ trans('language.priceUnit_ar') }}  </p>
                    <input name="priceUnit_ar"
                           class="form-control " type="text">

                    <p> {{ trans('language.priceUnit_en') }}  </p>
                    <input name="priceUnit_en"
                           class="form-control " type="text">


                           <p> {{ trans('language.typeSizeTitle_ar') }}  </p>
                    <input name="typeSizeTitle_ar"
                           class="form-control " type="text">

                    <p> {{ trans('language.typeSizeTitle_en') }}  </p>
                    <input name="typeSizeTitle_en"
                           class="form-control " type="text">

                        <div>
                            <p> {{ trans('language.earnest')}}</p>
                            <label>
                                <!-- <input type="radio" id="earnest_yes">-->
                                <input name="earnestSl" id="_yes" type="radio" value="عربون">عربون
                                <br>
                                <input name="earnestSl" id="_no" type="radio" value="لا عربون">لا يوجد عربون
                            </label>
                            <input name="earnest" class="form-control show-yes" type="text">
                        </div>

                        <br><br>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green"> {{ trans('language.add') }}</button>
                    <button type="button" data-dismiss="modal"
                            class="btn btn-outline dark"> {{ trans('language.close') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- stackable -->

        </div>
    </div>


        <!-- END CONTENT BODY -->

@endsection
