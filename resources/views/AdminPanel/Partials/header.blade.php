

@php

    
    $lang = LaravelLocalization::getCurrentLocale(); 

      App::setLocale($lang);

    $admin = Auth::user() ;
 

@endphp




@if($lang == "en" )
<html lang="en" dir="ltr">
@else
<html lang="en" dir="rtl">

@endif

<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="Admin Panel was made by Vision Tech developer Jick " name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> --}}
 
    @if($lang == 'ar')
       <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/simple-line-icons/simple-line-icons.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css')}}">
    <!-- END GLOBAL MANDATORY STYLES -->

    <link href="{{ asset('AdminPanelAssetFiles/pages/css/profile-rtl.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet"
          type="text/css"/>


    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css')}}"
          rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet"
          type="text/css"/>


    {{--  Modal part --}}
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    {{--  Modal part --}}

<!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('AdminPanelAssetFiles/global/css/components-rtl.min.css')}}" rel="stylesheet"
          id="style_components" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/css/plugins-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/layout-rtl.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/themes/darkblue-rtl.min.css')}}" rel="stylesheet"
          type="text/css" id="style_color"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/custom-rtl.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/layout-rtl.min.css')}}" rel="stylesheet"
          type="text/css"/>

    @else

      <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/simple-line-icons/simple-line-icons.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}">
    <!-- END GLOBAL MANDATORY STYLES -->

    <link href="{{ asset('AdminPanelAssetFiles/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet"
          type="text/css"/>


    <!-- BEGIN PAGE LEVEL PLUGINS -->

    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}"
          rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet"
          type="text/css"/>


    {{--  Modal part --}}
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    {{--  Modal part --}}

<!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('AdminPanelAssetFiles/global/css/components.min.css')}}" rel="stylesheet"
          id="style_components" type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/layout.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/themes/darkblue.min.css')}}" rel="stylesheet"
          type="text/css" id="style_color"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/custom.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('AdminPanelAssetFiles/layouts/layout/css/layout.min.css')}}" rel="stylesheet"
          type="text/css"/>


    @endif

   


    <link href="{{ asset('AdminPanelAssetFiles/global/plugins/cubeportfolio/css/cubeportfolio.css')}}" rel="stylesheet"
          type="text/css"/>


    {{--  Custom Css code  --}}
    <link href="{{ asset('AdminPanelAssetFiles/global/css/custommode.css')}}" rel="stylesheet" type="text/css"/>

    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>


    {{--  To make title of page cahnage in every page with it title  --}}
    <title>  @yield('page_title') </title>

    {{--  if you nedd to add some styles   --}}
    @yield('styles')
</head>
<!-- END HEAD -->




<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">


<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ url('/dash') }}">
                <img style="width: 10%; top:-10px;position: relative;"
                     src="{{ asset('AdminPanelAssetFiles/auth/images/ic_logo.png') }}" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler hidden-md hidden-lg ">
                <span class=""></span>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <img alt="" class="img-circle"
                             src="{{ asset('AdminPanelAssetFiles/default_user.jpg') }}"/>
                        <span class="username username-hide-on-mobile"> {{  $admin->name }} </span>
                        
                    </a>
                     
                </li>
                <li class="dropdown"> 


                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#">

                        
                                                 
                           <img width="20px"  src='{{ asset("AdminPanelAssetFiles/$lang-flag.png") }}' alt="">
                           <span style="color: #FFF"> {{ trans('language.lang') }}  </span>
                         

                        </a>

                            <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                                
                                

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>

                                        

                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                    <img width="20px"  src='{{ asset("AdminPanelAssetFiles/$localeCode-flag.png") }}' alt="">
                                                        {{ $properties['native'] }}
                                        </a>




                            


                                    </li>
                                @endforeach


                            </ul>
                     
                    </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="{{ url(app()->getLocale(). '/logout') }}" class="dropdown-toggle"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="icon-logout"></i>
                    </a>
                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>

            




     


 

 