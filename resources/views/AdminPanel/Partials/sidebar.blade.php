<style type="text/css">
    .color {
        transition: all 1s
    }

    .color:hover {
        color: #36c6d3
    }
</style>

@php
      $lang = LaravelLocalization::getCurrentLocale();
      App::setLocale($lang);
    $admin = Auth::user() ;

@endphp



 

    <div class="page-sidebar navbar-collapse collapse " >

     

    <!-- BEGIN SIDEBAR MENU -->

    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200" style="padding-top: 20px">
        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        {{--  {{ dd(Request::path()) }} --}}
        <li class="nav-item start color {{Request::path() == "dash"   ? 'active' : ''}} open" style="padding-top: 60px">
              
            <a href="{{ aurl("/dash") }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="color"> {{ trans('language.dashboard') }} </span>

                <span class="{{Request::path() == "dash"   ? 'selected' : ''}}"></span>
            </a>

        </li>


        <hr>
        <li class="nav-item start tags {{Request::path() == "users"   ? 'active' : ''}} ">
            <a href="{{aurl("/users")}}" class="nav-link nav-toggle">
                <i class=" fa fa-user"></i>
                <span class="title"> {{ trans('language.clients') }}  </span>
                <span class="{{Request::path() == "users"   ? 'selected' : ''}}"></span>
            </a>
        </li>
 

        <hr>

        <li class="nav-item start tags {{Request::path() == "sections"   ? 'active' : ''}}">
            <a href="{{aurl("/sections")}}" class="nav-link nav-toggle">
               
                <i class="fa fa-navicon"></i>
                <span class="title">  {{ trans('language.categories') }}  </span>
                <span class="{{Request::path() == "sections"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "subcategories"   ? 'active' : ''}}">
            <a href="{{aurl("/subcategories")}}" class="nav-link nav-toggle">

                <i class="fa fa-navicon"></i>
                <span class="title">  {{ trans('language.subCategories') }}  </span>
                <span class="{{Request::path() == "sections"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "products"   ? 'active' : ''}}">
            <a href="{{aurl("/products")}}" class="nav-link nav-toggle">
               
                <i class="fa fa-navicon"></i>
                <span class="title">  {{ trans('language.products') }}   </span>
                <span class="{{Request::path() == "products"   ? 'active' : ''}}"></span>
            </a>
        </li>
 

     
<hr>


<li class="nav-item start tags {{Request::path() == "conversations"   ? 'active' : ''}}">
            <a href="{{aurl("/conversations")}}" class="nav-link nav-toggle">
               
                <i class="fa fa-navicon"></i>
                <span class="title"> {{ trans('language.conversations') }}  </span>
                <span class="{{Request::path() == "conversations"   ? 'active' : ''}}"></span>
            </a>
        </li>


         <li class="nav-item start tags {{Request::path() == "notifications"   ? 'active' : ''}}">
            <a href="{{aurl("/notifications")}}" class="nav-link nav-toggle">
                <i class="fa fa-forumbee"></i>
                <span class="title"> {{ trans('language.notifications') }} </span>
                <span class="{{Request::path() == "notifications"   ? 'active' : ''}}"></span>
            </a>
        </li>


        <li class="nav-item start tags {{Request::path() == "orders"   ? 'active' : ''}}">
            <a href="{{aurl("/orders")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.orders') }}</span>
                <span class="{{Request::path() == "orders"   ? 'active' : ''}}"></span>
            </a>
        </li>



<hr>

        <li class="nav-item start tags {{Request::path() == "questions"   ? 'active' : ''}}">
            <a href="{{aurl("/questions")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.questions') }}</span>
                <span class="{{Request::path() == "questions"   ? 'active' : ''}}"></span>
            </a>
        </li>

         <li class="nav-item start tags {{Request::path() == "soicalMedias"   ? 'active' : ''}}">
            <a href="{{aurl("/soicalMedias")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.soicalMedias') }}</span>
                <span class="{{Request::path() == "soicalMedias"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "galleries"   ? 'active' : ''}}">
            <a href="{{aurl("/galleries")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.Gallery') }}</span>
                <span class="{{Request::path() == "galleries"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "cities"   ? 'active' : ''}}">
            <a href="{{aurl("/cities")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.cities') }}</span>
                <span class="{{Request::path() == "cities"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "areas"   ? 'active' : ''}}">
            <a href="{{aurl("/areas")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.areas') }}</span>
                <span class="{{Request::path() == "areas"   ? 'active' : ''}}"></span>
            </a>
        </li>


        <li class="nav-item start tags {{Request::path() == "termes"   ? 'active' : ''}}">
            <a href="{{aurl("/termes")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.termes') }} </span>
                <span class="{{Request::path() == "termes"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "sale_termes"   ? 'active' : ''}}">
            <a href="{{aurl("/sale_termes")}}" class="nav-link nav-toggle">
                <i class="fa fa-google-wallet"></i>
                <span class="title"> {{ trans('language.sale_termes') }} </span>
                <span class="{{Request::path() == "sale_termes"   ? 'active' : ''}}"></span>
            </a>
        </li>



        <li class="nav-item start tags {{Request::path() == "aboutMapsut"   ? 'active' : ''}}">
            <a href="{{aurl("/aboutMapsut")}}" class="nav-link nav-toggle">
                <i class="fa fa-unlink"></i>
                <span class="title"> {{ trans('language.about') }}  </span>
                <span class="{{Request::path() == "aboutMapsut"   ? 'active' : ''}}"></span>
            </a>
        </li>

        <li class="nav-item start tags {{Request::path() == "contacts"   ? 'active' : ''}}">
            <a href="{{aurl("/contacts")}}" class="nav-link nav-toggle">
                <i class="fa fa-unlink"></i>
                <span class="title">{{ trans('language.contacts') }} </span>
                <span class="{{Request::path() == "contacts"   ? 'active' : ''}}"></span>
            </a>
        </li>


        <hr>




 


    </ul>
    <!-- END SIDEBAR MENU -->
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
