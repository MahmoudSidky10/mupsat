</div>


<!-- BEGIN QUICK NAV -->

<div class="quick-nav-overlay"></div>

@php


    $lang = LaravelLocalization::getCurrentLocale(); 

      App::setLocale($lang);

    $admin = Auth::user() ;

@endphp



<!-- END QUICK NAV -->

<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->


<!-- BEGIN CORE PLUGINS -->
{{ Html::script('AdminPanelAssetFiles/global/plugins/jquery.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap/js/bootstrap.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/js.cookie.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jquery.blockui.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{ Html::script('AdminPanelAssetFiles/global/plugins/moment.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/morris/morris.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/morris/raphael-min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/counterup/jquery.waypoints.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/counterup/jquery.counterup.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/amcharts.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/serial.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/pie.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/radar.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/themes/light.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/themes/patterns.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amcharts/themes/chalk.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/ammap/ammap.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/amcharts/amstockcharts/amstock.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/fullcalendar/fullcalendar.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/flot/jquery.flot.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/flot/jquery.flot.resize.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/flot/jquery.flot.categories.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jquery.sparkline.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}
<!-- END PAGE LEVEL PLUGINS -->

{{--  modal part  --}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-modal/js/bootstrap-modal.js')}}

<!-- BEGIN THEME GLOBAL SCRIPTS -->
{{ Html::script('AdminPanelAssetFiles/global/scripts/app.min.js')}}

{{ Html::script('AdminPanelAssetFiles/global/scripts/bootstrap-selectsplitter.js')}}
<!-- END THEME GLOBAL SCRIPTS -->

{{ Html::script('AdminPanelAssetFiles/pages/scripts/ui-extended-modals.min.js')}}

<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{ Html::script('AdminPanelAssetFiles/global/scripts/dashboard.min.js')}}
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{ Html::script('AdminPanelAssetFiles/pages/scripts/profile.min.js')}}
<!-- END PAGE LEVEL SCRIPTS -->

{{ Html::script('AdminPanelAssetFiles/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}


{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-markdown/lib/markdown.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/bootstrap-summernote/summernote.min.js')}}



<!-- BEGIN PAGE LEVEL PLUGINS -->
{{ Html::script('AdminPanelAssetFiles/global/scripts/datatable.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/datatables/datatables.min.js')}}
{{ Html::script('AdminPanelAssetFiles/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}

{{ Html::script('AdminPanelAssetFiles/pages/scripts/table-datatables-responsive.min.js')}}
{{ Html::script('AdminPanelAssetFiles/layouts/layout/scripts/layout.min.js')}}
{{ Html::script('AdminPanelAssetFiles/layouts/layout/scripts/demo.min.js')}}
{{ Html::script('AdminPanelAssetFiles/layouts/global/scripts/quick-sidebar.min.js')}}
{{ Html::script('AdminPanelAssetFiles/layouts/global/scripts/quick-nav.min.js')}}


{{ Html::script('AdminPanelAssetFiles/pages/scripts/table-datatables-editable.min.js')}}


<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCICVFZg9PawAeVO5oH_BRdE7IEu93eG8E&libraries=places"></script>
{{ Html::script('AdminPanelAssetFiles/global/scripts/setMap.js')}}


<script src="//cdn.jsdelivr.net/bootstrap.selectsplitter/0.1.4/bootstrap-selectsplitter.min.js"></script>


<!-- END THEME LAYOUT SCRIPTS -->

@if($lang == "en")


    {{ Html::script('AdminPanelAssetFiles/global/scripts/customode_en.js')}}

@else
    {{ Html::script('AdminPanelAssetFiles/global/scripts/customode.js')}}

@endif

<script>
    $(document).ready(function () {
        $('#clickmewow').click(function () {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();


    });
</script>


<script>

    $(".selectall").click(function () {

        $(".individual").prop("checked", $(this).prop("checked"));
    });


    phone = $("#mobNum");

    phone.on('keypress', function (e) {


        filter = /^05/;
        filter2 = /^5/;
        if ($(this).val().length > 1) {
            if (filter.test($(this).val())) {
                $(this).css('border', '1px solid green');
            } else if (filter2.test($(this).val())) {

            } else {
                $(this).css('border', '1px solid red');


                $(this).tooltip().mouseover();
                return false;
            }
        } else {
            $(this).tooltip('disable');
            return true;
        }
        if ($(this).val().length > 9) {
            return false;
        }
    });


    $("#pointer").click(function () {
        console.log("hello");
    });

    //display checkbox
    $('#_yes').change(function () {
        if ($(this).is(':checked')) {
            $('.show-yes').fadeIn();
        }
    })
    $('#_no').change(function () {
        if ($(this).is(':checked')) {
            $('.show-yes').fadeOut();
        }
    })

    /* show  price and hide*/
    $('#type_yes').change(function () {
        if ($(this).is(':checked')) {
            $('.hold-sub-price > div').fadeOut(100);
            $('.hold-sub-price .subcategoriesList').removeClass('hide');

            var v = $(this).closest("div.typesList").find("input[name=’rank’]").val();

        } else {
            $('.hold-sub-price .subcategoriesList').addClass('hide');
            $('.hold-sub-price .productPrice').fadeIn(100);
        }
    })


</script>


<script type="text/javascript">

    $(".category_id").on('change', function () {


        $(".typesList").empty();
        $.post("{{url("/getSubCategories")}}",
            {
                category_id: $(this).val()
            },
            function (data, status) {
                $.each(data, function () {
                    $.each(this, function (index, item) {
                        $(".typesList").prepend('' +
                            '<div class="subCategoriesContainer">' +
                            '<input class="subCat" type="checkbox" id="subCat' + item.id + '" name="subCats[]" value="' + item.id + '"> ' +
                            '<div class="packet">' +
                            '<label for="subCat' + item.id + '"> ' + item.name + '  </label> ' +
                            '<input oninput="" class="inputshow form-control" type="text"  name="productprice[]" placeholder="السعر" value="">' +
                            '</div>' +
                            '</div>'
                        );
                    });
                });
                $('.loader').fadeOut();
            });


    });


    $(document).on("click", ".subCat", function () {
        if ( this.checked ) {
            this.parentElement.querySelector('.inputshow').setAttribute("required", "required");
        } else {
            this.parentElement.querySelector('.inputshow').removeAttribute("required");
        }
    });


</script>


@yield('scripts')

</body>

</html>
