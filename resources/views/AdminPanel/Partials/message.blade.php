@if(Session::has('success'))

 <div class="alert alert-success alert-dismissable text-center" 

style="background-color: #36c6d3;
    border-color: #337ab7;
    color: #ffffff;" 
 >
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('success') }}
 </div>

@endif