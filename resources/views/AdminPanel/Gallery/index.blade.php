@php

$lang = LaravelLocalization::getCurrentLocale();
App::setLocale($lang);
@endphp
@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
<!-- BEGIN PAGE TITLE-->


<!-- BEGIN PAGE TITLE-->
<h1 style="padding-top: 60px; margin-right: 20px;color: #fff" class="page-title">
<small style="color: #FFF"></small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
					<br>
					<br>
					<a class="btn btn-outline green" data-toggle="modal" href="#Addresponsive">{{ trans('language.add') }} </a>
				</div>
				
			</div>
			@if($galleries)
			
			
			@foreach($galleries as $gallerie)
			<div class="col-md-3" style="margin-bottom: -50px; margin-top: 30px;">
				<img style="margin: 5px;" width="295px" height="300px" data-toggle="modal"
				data-target=".image_modal"
				class="view_image "
				
				@if($gallerie->image)
				src="{{$gallerie->image}}"
				
				@else
				src="{{ asset('AdminPanelAssetFiles/section.png') }}"
				@endif
				>
			 

				<button
						
						@if($lang == "en")
				  style="position: relative;
					top: -50px;
				left: 20px;" 
				@else

				style="position: relative;
					top: -50px;
				right: 20px;"

				@endif

				 data-id="" data-toggle="modal"  data-target="#delete_modal{{ $gallerie->id }}"
                                class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                                {{  trans('language.delete') }}
                </button>

                  {{--  DELETE Form    --}}
                                <div id="delete_modal{{ $gallerie->id }}" class="modal fade delete_modal bgmodal">
                                    <div class="modal-dialog modal-confirm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="icon-box">
                                                    <i class="material-icons">&#xE5CD;</i>
                                                </div>
                                                <h4 class="modal-title">{{ trans('language.delete_danger') }}</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <p> {{ trans('language.delete_aleart') }}    </p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['route'=>["galleries.destroy" , $gallerie->id ], 'class'=>"" ]) !!}
                                                {!! method_field('DELETE') !!}
                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{ trans('language.close') }}</button>
                                                
                                                
                                                <button  type="submit" class="btn btn-danger">{{ trans('language.delete') }}</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

			</div>
			
			@endforeach


			
			@else
			no images
			@endif
		</div>

	</div>

</div>


	<br>
			<br>
			<br>


      <!-- responsive -->
            <div id="Addresponsive" class="modal fade" tabindex="-1" data-width="760">
                {!! Form::open( array('route' => array('galleries.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'AddForm'))  !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4> {{ trans('language.add') }} {{ trans('language.image') }}</h4>
                 
                </div>
                <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                 
                                                                    <div class="form-group form-md-line-input">
                                                <div class="col-xs-12 label">
                                                    <label for="image">{{__('language.image ')}}</label>
                                                </div>

                                                <div class="input">
                                                    <img id="upload" src="{{ asset('AdminPanelAssetFiles/section.png') }}" class="upload" width="100" height="100" >

                                                </div>



                                                <input required="required"  class="image" name="image" type="file">
                                            </div>
                                                <p> {{trans('language.language')}}  </p>
                                                    <select name="lang" class="form-control">
                                                        <option value="ar"> {{trans('language.arabic')}} </option>
                                                        <option value="en"> {{trans('language.english')}} </option>
                                                    </select>

                                                
                                            </div>
                                        </div>
                                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green"> {{ trans('language.add') }}</button>
                    <button type="button" data-dismiss="modal" class="btn btn-outline dark"> {{ trans('language.close') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- stackable -->


@endsection
