 @php

$lang = LaravelLocalization::getCurrentLocale();
App::setLocale($lang);
use Carbon\Carbon;
 Carbon::setLocale($lang);

 @endphp
@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE TITLE-->
<h1 style="padding-top: 60px;color: #FFF" class="page-title"> {{ trans('language.show_conversations') }}
</h1>


<div class="row">
 	
 	@if($conversations)
 	@foreach($conversations as $conversation)
<div class="col-md-4" >
	<div class="portlet light portlet-fit bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class=" icon-layers font-green"></i>
				<span class="caption-subject font-green bold uppercase">

				 
				 
					@php $reserver_id = $conversation->id @endphp
				 <a class="uppercase" href="{{ url("/openConversation/$reserver_id") }}">

				  {{ $conversation->receiver->name }}
				</a>

				  </span>
				<div class="caption-desc font-grey-cascade"> </div>
			</div>
		</div>

		<div class="portlet-body">
			<div class="mt-element-overlay">
				<div class="row">
					<div class="col-md-12"  >
						<div class="mt-overlay-5"  style="max-height: 300px">


							  
							<img height="100%" width="100%" src="{{ $conversation->receiver->image }}" />
							<div class="mt-overlay">
								<h2 style="direction: ltr;">{{ $conversation->created_at->diffForHumans() }}</h2>
								<p>
									@php $reserver_id = $conversation->id @endphp


									<a class="uppercase" href="{{ url("/openConversation/$reserver_id") }}">

										{{ trans('language.open_conversations') }}
									    </a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
 
	</div>
</div>
@endforeach
	
	@else

	<h3> لا توجد محادثات سابقه </h3>
	@endif
</div>

@endsection