@php


    $lang = LaravelLocalization::getCurrentLocale();

         App::setLocale($lang);
@endphp

@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px;color: #FFF" class="page-title">  {{ trans('language.show') }}
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">عرض البيانات</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal" href="#Addresponsive"> اضافه منتج </a>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id=" ">
                        <thead>
                        <tr>
                            <td> #</td>
                            <td class="all"> {{ trans('language.name_ar') }} </td>
                            <td class="all"> {{ trans('language.name_en') }} </td>
                            <td class="all"> {{ trans('language.details_ar') }} </td>
                            <td class="all"> {{ trans('language.details_en') }} </td>
                            <td class="all"> {{ trans('language.price') }} </td>
                            <td class="all"> {{ trans('language.rate') }} </td>
                            <td class="all"> {{ trans('language.category') }} </td>
                            <td class="all"> {{ trans('language.code') }} </td>
                            <td class="all"> {{ trans('language.size_ar') }} </td>
                            <td class="all"> {{ trans('language.size_en') }} </td>
                            <td class="all"> {{ trans('language.minimumCount') }} </td>
                            <td class="all"> {{ trans('language.maximumCount') }} </td>
                            <td class="all"> {{ trans('language.increasingBy') }} </td>
                            <td class="all"> {{ trans('language.settings') }} </td>
                        </tr>
                        </thead>
                        <tbody>
                        @if($products->count())
                            @foreach($products as $product)

                                {{-- Edit Modal --}}
                                <div id="Editresponsive{{$product->id}}" class="modal fade" tabindex="-1"
                                     data-width="760" data-height="700">
                                    {!! Form::open( array('route' => array('products.update',$product->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'editform'))  !!}
                                    {!! method_field('put') !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="form-group form-md-line-input">
                                                    <div class="col-xs-12 label">
                                                        <label for="image">{{ trans('language.image') }}</label>
                                                    </div>

                                                    <div class="input">
                                                        <img id="upload" class="upload" width="100" height="100"

                                                             @if($product->image)

                                                             src="{{$product->image}}"

                                                             @else

                                                             src="{{ asset('AdminPanelAssetFiles/section.png') }}"

                                                            @endif

                                                        >
                                                    </div>

                                                    <input class="image" name="image" type="file">


                                                </div>

                                                <div class="form-group form-md-line-input">
                                                    <div class="col-xs-12 label">
                                                        <label for="image">{{ trans('language.cover_image') }}</label>
                                                    </div>

                                                    <div class="input">
                                                        <img id="upload" class="upload" width="100" height="100"

                                                             @if($product->cover_image)

                                                             src="{{$product->cover_image}}"

                                                             @else

                                                             src="{{ asset('AdminPanelAssetFiles/section.png') }}"

                                                            @endif

                                                        >
                                                    </div>

                                                    <input class="image" name="cover_image" type="file">


                                                </div>


                                                <p> {{ trans('language.name_ar') }}  </p>
                                                <input name="name_ar" value="{{ $product->name_ar }}"
                                                       class="form-control" type="text">

                                                <p> {{ trans('language.name_en') }}  </p>
                                                <input name="name_en" value="{{ $product->name_en }}"
                                                       class="form-control" type="text">

                                                <p> {{  trans('language.details_ar') }} </p>
                                                <input name="details_ar" value="{{ $product->details_ar }}"
                                                       class="form-control"
                                                       type="text">

                                                <p> {{  trans('language.minimumCount') }} </p>
                                                <input name="minimumCount" value="{{ $product->minimumCount }}"
                                                       class="form-control"
                                                       type="number">
                                                <p> {{  trans('language.maximumCount') }} </p>
                                                <input name="maximumCount" value="{{ $product->maximumCount }}"
                                                       class="form-control"
                                                       type="number">

                                                <p> {{  trans('language.increasingBy') }} </p>
                                                <input name="increasingBy" value="{{ $product->increasingBy }}"
                                                       class="form-control"
                                                       type="number">

                                                <p> {{  trans('language.details_en') }} </p>
                                                <input name="details_en" value="{{ $product->details_en }}"
                                                       class="form-control"
                                                       type="text">

                                                @if(count($product->getsubCategories()) <= 0 )
                                                    <p> {{ trans('language.price') }}  </p>
                                                    <input name="price" value="{{ $product->price }}"
                                                           class="form-control " type="text">
                                                @endif

                                                <p> {{ trans('language.category') }} </p>

                                                <select name="section_id" class="form-control">
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"
                                                            {{ $product->section_id  == $category->id ? 'selected' :  ''  }}

                                                        > {{ $category->name_ar }} </option>
                                                    @endforeach
                                                </select>

                                                @if(count($product->getsubCategories()) > 0)

                                                    <p> {{  trans('language.showsubcatPrices') }} </p>


                                                    @foreach($product->getsubCategories() as $subcategory)
                                                        <br>
                                                        <label
                                                            class="label label-info">   @if($lang == "en")  {{  $subcategory->subcat->name_en  }}  @else {{  $subcategory->subcat->name_ar  }}   @endif </label>



                                                        <br>
                                                        <input type="hidden" name="productids[]"
                                                               value="{{  $subcategory->id  }}">
                                                        <input oninput="" class="inputshow form-control" type="text"
                                                               name="productprice[]" placeholder="السعر"
                                                               value="{{  $subcategory->price  }}">
                                                        <br>
                                                    @endforeach

                                                @endif


                                                <p> {{ trans('language.rate') }}  </p>
                                                <input name="rate" required="required" value="{{ $product->rate }}"
                                                       class="form-control " type="text">

                                                <p> {{ trans('language.citycode') }}  </p>
                                                <input name="code" required="required" value="{{ $product->code }}"
                                                       class="form-control " type="text">

                                                <p> {{ trans('language.size_ar') }}  </p>
                                                <input name="size_ar" value="{{ $product->size_ar }}"
                                                       class="form-control " type="text">

                                                <p> {{ trans('language.size_en') }}  </p>
                                                <input name="size_en" value="{{ $product->size_en }}"
                                                       class="form-control " type="text">



                                                <div class="">
                                                    <br>
                                                    <label style="font-size: 14px" class="control-label"> {{ trans('language.is_featured') }} ! </label>
                                                    <div class="controls col-md-12">
                                                        <select name="special" class="form-control">
                                                            <option {{ $product->special  == 1 ? 'selected' :  ''  }} value="1"> Yes </option>
                                                            <option {{ $product->special  == 0 ? 'selected' :  ''  }}  value="0"> No </option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn green"> {{  trans('language.edit') }}</button>
                                        <button type="button" data-dismiss="modal"
                                                class="btn btn-outline dark"> {{  trans('language.close') }}
                                        </button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <tr>
                                    <td class="centerCollom">{{ $loop->iteration }}</td>
                                    <td class="centerCollom">{{ $product->name_ar }}</td>
                                    <td class="centerCollom">{{ $product->name_en }}</td>
                                    <td class="centerCollom">{{ $product->details_ar }}</td>
                                    <td class="centerCollom">{{ $product->details_en }}</td>
                                    <td class="centerCollom">{{ $product->price }}</td>
                                    <td class="centerCollom">{{ $product->rate }}</td>
                                    <td class="centerCollom">{{ $product->formate_section_name }}</td>
                                    <td class="centerCollom">{{ $product->code }}</td>
                                    <td class="centerCollom">{{ $product->size_ar }}</td>
                                    <td class="centerCollom">{{ $product->size_en }}</td>
                                    <td class="centerCollom">{{ $product->minimumCount }}</td>
                                    <td class="centerCollom">{{ $product->maximumCount }}</td>
                                    <td class="centerCollom">{{ $product->increasingBy }}</td>
                                    <td>
                                        <ul class="btnsList  "
                                            style="padding-left: 0px!important;padding-right: 20px;padding-top: 5px;">
                                            <li>
                                                {{-- responsive{{$region->id}} --}}
                                                <a title="تعديل بيانات المستخدم" class="btn btn-success"
                                                   data-toggle="modal"
                                                   href="#Editresponsive{{$product->id}}">
                                                    <i class="fa fa-edit"></i> {{ trans('language.edit') }} </a>
                                            </li>
                                            <li>
                                                <button data-id="" data-toggle="modal"
                                                        data-target="#delete_modal{{ $product->id }}"
                                                        class="btn btn-danger">
                                                    <i class="fa fa-remove"></i>
                                                    {{ trans('language.delete') }}
                                                </button>
                                                {{--  DELETE Form    --}}
                                                <div id="delete_modal{{ $product->id }}"
                                                     class="modal fade delete_modal bgmodal">
                                                    <div class="modal-dialog modal-confirm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <div class="icon-box">
                                                                    <i class="material-icons">&#xE5CD;</i>
                                                                </div>
                                                                <h4 class="modal-title">{{ trans('language.delete_danger') }}  </h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p> {{ trans('language.delete_aleart') }} ( <span
                                                                        style="color: #f15e5e">  {{$product->name_ar}} </span>
                                                                    ) ؟ </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                {!! Form::open(['url'=>["products" , $product->id ], 'class'=>"" ]) !!}
                                                                {!! method_field('DELETE') !!}
                                                                <button type="submit"
                                                                        class="btn btn-danger"> {{ trans('language.delete') }}</button>
                                                                <button type="button" class="btn btn-info"
                                                                        data-dismiss="modal">

                                                                    {{ trans('language.close') }}
                                                                </button>
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('language.empty') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $products->links()  }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- responsive -->
    <div id="Addresponsive" class="modal fade" tabindex="-1" data-width="760" data-height="700">
        {!! Form::open( array('route' => array('products.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'AddForm'))  !!}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> اضافه منتج جديد </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">


                    <p> {{ trans('language.name_ar') }}  </p>
                    <input name="name_ar"
                           class="form-control" type="text">

                    <p> {{ trans('language.name_en') }}  </p>
                    <input name="name_en"
                           class="form-control" type="text">

                    <p> {{  trans('language.details_ar') }} </p>
                    <input name="details_ar" class="form-control"
                           type="text">

                    <p> {{  trans('language.minimumCount') }} </p>
                    <input name="minimumCount" class="form-control"
                           type="number">


                    <p> {{  trans('language.maximumCount') }} </p>
                    <input name="maximumCount" class="form-control"
                           type="number">

                    <p> {{  trans('language.increasingBy') }} </p>
                    <input name="increasingBy" class="form-control"
                           type="number">


                    <p> {{  trans('language.details_en') }} </p>
                    <input name="details_en" class="form-control"
                           type="text">

                    <p> {{ trans('language.rate') }}  </p>
                    <input name="rate" required="required"
                           class="form-control " type="number">


                    <p> {{ trans('language.code') }}  </p>
                    <input name="code" required="required"
                           class="form-control " type="text">

                    <p> {{ trans('language.size_ar') }}   </p>
                    <input name="size_ar"
                           class="form-control " type="text">

                    <p> {{ trans('language.size_en') }}  </p>
                    <input name="size_en"
                           class="form-control " type="text">


                    <p> {{ trans('language.category') }} </p>

                    <select name="section_id" class="form-control category_id">
                        <option value="0"> اختار القسم الرئيسي</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}"> {{ $category->name }} </option>
                        @endforeach
                    </select>
                    <div>
                        <p> {{ trans('language.types')}}</p>
                        <label>
                            <!-- <input type="radio" id="earnest_yes">-->
                            <input name="typeyes" id="type_yes" type="checkbox"
                                   value="0"> {{ trans('language.productTypes') }}
                        </label>

                        <div class="hold-sub-price">

                            <div class="col-md-12 subcategoriesList hide">

                                <p> {{  trans('language.subCategories')  }}  </p>

                                <div class="col-md-2 typesList">

                                </div>


                                <br>
                                <br>
                            </div>
                            <hr>

                            <div class="col-md-12 productPrice ">
                                <p> {{ trans('language.price') }}  </p>
                                <input name="price"
                                       class="form-control " type="text">
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                    <p>{{ trans('language.image') }} </p>
                    <input required="required" class="image" name="image" type="file">

                    <p> {{ trans('language.cover_image') }} </p>
                    <input required="required" class="image" name="cover_image" type="file">


                    <div class="">
                        <br>
                        <label style="font-size: 14px" class="control-label"> {{ trans('language.is_featured') }} ! </label>
                        <div class="controls col-md-12">
                            <select name="special" class="form-control">
                                <option value="1"> Yes </option>
                                <option value="0"> No </option>
                            </select>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn green"> {{trans('language.add')}}</button>
            <button type="button" data-dismiss="modal"
                    class="btn btn-outline dark">  {{trans('language.close')}} </button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection


@section('scripts')
    <script>


    </script>
@endsection
