@php

    $lang = LaravelLocalization::getCurrentLocale();
          App::setLocale($lang);

@endphp




@extends('AdminPanel.app')


@section('page_title',  trans('language.dashboard') )
@section('content')

    <h1 style="padding-top: 60px;color: #FFF" class="page-title"> {{  trans('language.termes') }}
        <small style="color: #FFF"> </small>
    </h1>






    <div class="row" style="background: #FFF;">
        <div class="col-sm-10 col-xs-10">
            <section class="m-t-40">
                <div class="sttabs tabs-style-iconbox">

                    {!! Form::model( $termes ,  ['route'=>['termes_update' , $termes->id],'method'=>'POST','class'=>'form-horizontal ','role'=>'form','files'=> true , 'id'=>'article_update']) !!}

                    {!! method_field('put') !!}





                    <div class="form-group col-md-12">
                        <label for="">{{ trans('language.text_ar') }}</label>
                        <div class="input-group">

                            <textarea style="height: 293px; width: 1337px; margin: 0px; padding: 0"  id="name_ar" name="termes_ar" placeholder="Termes and conditions "  >{{ $termes->termes_ar }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="">{{ trans('language.text_en') }}</label>
                        <div class="input-group">

                            <textarea style="height: 293px; width: 1337px; margin: 0px; padding: 0"  id="text_en" name="termes_en" placeholder="Termes and conditions "  >{{ $termes->termes_en }}</textarea>
                        </div>
                    </div>





                <div class="form-group col-md-12">
                    <div class="col-md-12">

                        <button type="submit" id="submit" class="btn btn-info btn-circle btn-lg"  data-toggle="tooltip" data-original-title="تحديث"><i class="fa fa-check"></i> </button>


                    </div>
                </div>



            {!! Form::close() !!}



        </div>
        </section>
    </div>
    <br>






@endsection