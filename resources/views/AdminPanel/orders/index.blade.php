@php
use App\User;
use App\Product;
use App\Section;
 
 
$products = Product::all();
$sections = Section::all();
$clients = User::all();

$lang = LaravelLocalization::getCurrentLocale();
App::setLocale($lang);
@endphp
@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE TITLE-->
<h1 style="padding-top: 60px;color: #FFF" class="page-title">  {{ trans('language.show') }}
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
<div class="portlet-title">
<div class="caption font-dark">
    <i class="icon-settings font-dark"></i>
    <span class="caption-subject bold uppercase">{{ trans('language.show') }} </span>
    <br>
    <br>
    <br>
    
<!--     <a class="btn btn-outline green" data-toggle="modal" href="#addTechnicianModal"> اضافه   طلب  </a> -->
</div>
<div class="tools"></div>
</div>
<div class="portlet-body">
<table class="  table table-striped table-bordered table-hover dt-responsive"
    style="text-align: center;" width="100%" id="myTable">
    <thead>
        <tr>
            <td>#</td>

            <td> {{ trans('language.image') }} </td>
          
            <td> {{ trans('language.client') }} </td>
            <td> {{ trans('language.status') }} </td>

            <td> {{ trans('language.total_price') }} </td>
            <td> {{ trans('language.totalEarnestOfOrder') }} </td>
            <td> {{ trans('language.orderResidual') }} </td>
      

 
            <td> {{ trans('language.time') }} </td>
            
            <td> {{ trans('language.settings') }} </td>
        </tr>
    </thead>
    <tbody>
        @if($orders->count())
        @foreach($orders as $order)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>
                <img style="margin-top: 0px; border-radius: 10px !important"  width="50px" height="50px" data-toggle="modal"
                data-target=".image_modal"
                class="view_image img-circle"
                @if($order->image)
                src="{{$order->image}}"
                @else
                src="{{ asset('AdminPanelAssetFiles/default_user.jpg') }}">
                @endif
            </td>
            
            
            <td>{{ $order->user->first_name }} {{ $order->user->last_name }}</td>
 
            <td>{{ $order->order_status->name}}</td>

            <td>{{ $order->totalPriceOfOrder}}</td>
            <td>{{ $order->totalEarnestOfOrder}}</td>
            <td>{{ $order->orderResidual}}</td>
 
 
            <td>{{ $order->created_at  }}  </td>

            

 
            
            <td>
                <ul class="btnsList" style="padding-left: 0px!important;padding-right: 20px;">
                    
                     <li>
                                                
                                                <a title="" class="btn btn-success"
                                                    
                                                   href="{{aurl("orderProducts/$order->id")}}">
                                                    <i class="fa fa-edit"></i> {{ trans('language.products') }} </a>
                     </li>

                    <li>

                        <a title="" class="btn btn-info"

                           href="{{aurl("orderBankInformation/$order->id")}}">
                            <i class="fa fa-edit"></i> {{ trans('language.orderBankInformation') }} </a>
                    </li>

                    
 
                    <li>
                        <button data-id="" data-toggle="modal"
                        data-target="#delete_modal{{ $order->id }}"
                        class="btn btn-danger">
                        <i class="fa fa-remove"></i>
                        {{ trans('language.delete') }}
                        </button>
                        {{--  DELETE Form    --}}
                        <div id="delete_modal{{ $order->id }}"
                            class="modal fade delete_modal bgmodal">
                            <div class="modal-dialog modal-confirm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="icon-box">
                                            <i class="material-icons">&#xE5CD;</i>
                                        </div>
                                        <h4 class="modal-title">{{ trans('language.delete_danger') }}</h4>
                                        <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{url("deleteorder/$order->id")}}">
                                            <button type="button" class="btn btn-info"
                                            data-dismiss="modal">{{ trans('language.close') }}</button>
                                            <button type="submit"
                                            class="btn btn-danger">{{ trans('language.delete') }}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
        @endforeach
        @else
        {{-- show than no data yet for this User .. --}}
        <tr>
            <td colspan="30"
            class="text-center">{{ trans('language.no_data') }}</td>
        </tr>
        @endif
    </tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<div id="addTechnicianModal" class="modal fade delete_modal bgmodal ">
<form id="addTechnicianForm" action="{{ url('/storeOrder') }}" method="post">
<div class="modal-dialog modal-confirm">
<div class="modal-content">
<div class="modal-header">
    <h3> {{ trans('language.add_order') }}  </h3>
    <hr>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <div class="col col-md-12">
        <div class="col-md-6">
            <label> {{ trans('language.sections') }}  </label>
            <select id="familys" name="section_id" class="form-control">
                <option value="item0">
                    -- {{ trans('language.sections') }} --
                </option>
                @foreach($sections as $section)
                <option value="{{ $section->id }}"> {{ $section->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label> {{  trans('language.products') }}  </label>
            <select id="products" name="product_id" class="form-control"
                style="width: 150px">
                <option value="item0">
                    -- {{  trans('language.products') }}--
                </option>
            </select>
        </div>
        <br>
    </div>
    <br>
    <br>
    <br>
    <br>
    <p >  {{ trans('language.clients') }}</p>
    <select name="user_id" class="form-control">
        @foreach($clients as $client)
        <option value="{{ $client->id }}" > {{ $client->first_name }} {{ $client->last_name }} </option>
        @endforeach
    </select>
    <p > {{ trans('language.quantity') }} </p>
    <input type="text" name="Quantity" required="required" placeholder=" {{ trans('language.quantity') }} " class="form-control">

    <p > {{ trans('language.Amount_provided') }}</p>
    <input type="text" name="Amount_provided" required="required" placeholder="{{ trans('language.Amount_provided') }}" class="form-control">
    
    
    <br>
 

   
    <div class="modal-footer">
        <button type="submit"
        class="btn green"> {{ trans('language.add') }}</button>
        <button type="button" data-dismiss="modal"
        class="btn red"> {{ trans('language.close') }}</button>
    </div>
</div>
</div>
</div>
</form>
</div>
<script type="text/javascript">
$(document).ready(function () {
$("#familys").change(function () {
var val = $(this).val();
$.ajax({
url: 'getProducts',
type: 'GET',
data: {id: val},
success: function (data) {
$('#products').html("");
$.each(data, function () {
$("#products").prepend('' +
'<option  value="0">' + '-- اختيار المنتج  --' + '</option>');
$.each(this, function (index, item) {
console.log(item);
$("#products").prepend('' +
'<option  value="' + item.id + '">' + item.name_ar + '</option>');
});
});
}
}); // end ajax function
});

});
</script>
<script>
$('.addTechnicianBtn').click(function () {
order_id = $(this).data('order_id');
url = "{{url("AddTechorder/")}}"+"/" + order_id;
$('#addTechnicianForm').attr('action', url);
});

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>

@endsection