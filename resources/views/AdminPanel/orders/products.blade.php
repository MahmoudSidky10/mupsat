@php


    $lang = LaravelLocalization::getCurrentLocale();

         App::setLocale($lang);
@endphp

@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px;color: #FFF" class="page-title">  {{ trans('language.show') }}
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">عرض البيانات</span>
                      
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <td> #</td>
                            {{--<td class="all" > {{ trans('language.image') }} </td>--}}
                            <td class="all"> {{ trans('language.count') }} </td>
                            <td class="all"> {{ trans('language.total_price') }} </td>
                            <td class="all"> {{ trans('language.name') }} </td>

                            <td class="all"> {{ trans('language.details') }} </td>

                            <td class="all"> {{ trans('language.price') }} </td>
                            <td class="all"> {{ trans('language.rate') }} </td>
                            <td class="all"> {{ trans('language.category') }} </td>
                            <td class="all"> {{ trans('language.code') }} </td>
                            <td class="all"> {{ trans('language.size') }} </td>


                            <td class="all"> {{ trans('language.minimumCount') }} </td>

                            <td class="all"> {{ trans('language.special') }} </td>
                           
                        </tr>
                        </thead>
                        <tbody>
                        @if($products->count())
                            @foreach($products as $product)

                                
                                <tr>
                                    <td class="centerCollom">{{ $loop->iteration }}</td>



                                    <td class="centerCollom">

                                        @php

                                           echo  $getprosuctinorderCount = \App\OrderProducts::where('order_id',$orderId)
                                            ->where('product_id',$product->product->id)->first()->count;


                                        @endphp

                                    </td>

                                    <td class="centerCollom">

                                        @php

                                            $getprosuctinorderCount = \App\OrderProducts::where('order_id',$orderId)
                                             ->where('product_id',$product->product->id)->first()->count;

                                         $getprosuctinorderPrice = \App\OrderProducts::where('order_id',$orderId)
                                             ->where('product_id',$product->product->id)->first()->productPrice;

                                        echo $getprosuctinorderCount * $getprosuctinorderPrice ;


                                        @endphp

                                    </td>



                                    @if(app()->getLocale() == "en")
                                        <td class="centerCollom">{{ $product->product->name_en }}</td>
                                        @else
                                        <td class="centerCollom">{{ $product->product->name_ar }}</td>
                                     @endif

                                    @if(app()->getLocale() == "en")
                                        <td class="centerCollom">{{ $product->product->details_en }}</td>
                                    @else
                                        <td class="centerCollom">{{ $product->product->details_ar }}</td>
                                    @endif

                                    
                                    <td class="centerCollom">
                                        @php
                                        echo  $getprosuctinorderCount = \App\OrderProducts::where('order_id',$orderId)
                                        ->where('product_id',$product->product->id)->first()->productPrice;
                                        @endphp
                                    </td>
                                    <td class="centerCollom">{{ $product->product->rate }}</td>
                                    <td class="centerCollom">{{ $product->product->section->name }}</td>
                                    <td class="centerCollom"> #{{ $product->product->code }}</td>
                                    @if(app()->getLocale() == "en")
                                        <td class="centerCollom">{{ $product->product->size_en }}</td>
                                    @else
                                        <td class="centerCollom">{{ $product->product->size_ar }}</td>
                                    @endif

                                    <td class="centerCollom">{{ $product->product->minimumCount }}</td>

                                    <td class="centerCollom">
                                        @if( $product->product->special == "0" )
                                            <span
                                                class="label label-primary"> {{ trans('language.special_no') }}  </span>
                                        @elseif($product->product->special == "1"  )
                                            <span
                                                class="label label-primary"> {{ trans('language.special_yes') }}  </span>
                                        @endif


                                    </td>


                                     
                                </tr>
                            @endforeach
                        @else
                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('language.empty') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

 
@endsection
