

@php





    $lang = LaravelLocalization::getCurrentLocale();

  App::setLocale($lang);


@endphp

@extends('AdminPanel.app')


@section('page_title',  trans('language.dashboard') )

@section('content')


    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px;color: #FFF" class="page-title"> {{  trans('language.show') }}
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal" href="#responsive"> {{ trans('language.add') }}  </a>

                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table  class="  table table-striped table-bordered table-hover dt-responsive"
                            style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>

                            <td class="all" > {{  trans('language.fromBank')  }} </td>
                            <td class="all"> {{  trans('language.accountName')  }} </td>
                            <td class="all"> {{  trans('language.accountNumber')  }} </td>
                            <td class="all"> {{  trans('language.transferAmount')  }} </td>
                            <td class="all"> {{  trans('language.requestImage')  }} </td>
                            <td class="all"> {{  trans('language.confirmMessage')  }} </td>

                        </tr>
                        </thead>
                        <tbody>

                        @if($bankinfo->count())

                                <tr>

                                    <td>{{ $bankinfo->fromBank }}</td>
                                    <td>{{ $bankinfo->accountName }}</td>
                                    <td>{{ $bankinfo->accountNumber }}</td>
                                    <td>{{ $bankinfo->transferAmount }}</td>
                                    <td>

                                        <img width="50px" height="50px" data-toggle="modal"
                                             data-target=".image_modal"
                                             class="view_image img-circle"
                                             @if($bankinfo->requestImage)

                                             src="{{$bankinfo->requestImage}}"

                                             @else

                                             src="{{ asset('AdminPanelAssetFiles/section.png') }}">

                                        @endif

                                    </td>


                                    <td>{{ $bankinfo->confirmMessage }}</td>



                                </tr>




                        @else

                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('no_data') }}</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>




@endsection
