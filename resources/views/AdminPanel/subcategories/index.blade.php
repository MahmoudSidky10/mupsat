@php


    $lang = LaravelLocalization::getCurrentLocale();

         App::setLocale($lang);
@endphp


@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')

    <!-- BEGIN CONTENT BODY -->

    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px; margin-right: 20px;color: #fff" class="page-title">
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal"
                           href="#Addresponsive">{{ trans('language.add') }} </a>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>


                            <th class="all"> #</th>

                            <th class="all">  {{ trans('language.name_ar') }} </th>
                            <th class="all">  {{ trans('language.name_en') }} </th>

                            <th class="all">  {{ trans('language.category') }} </th>
                            <th class="all">  {{ trans('language.settings') }} </th>

                        </tr>


                        </thead>
                        <tbody>

                        @foreach($subCategories as $subCategory)


                            {{-- Edit Modal --}}
                            <div id="Editresponsive{{$subCategory->id}}" class="modal fade" tabindex="-1" data-width="760">
                                {!! Form::open( array('route' => array('subcategories.update',$subCategory->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>''))  !!}
                                {!! method_field('put') !!}


                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>

                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-md-12">

                                            <h4> {{ trans('language.show') }} </h4>


                                            <p> {{ trans('language.name_ar') }}  </p>
                                            <input name="name_ar" value="{{ $subCategory->name_ar }}" class="form-control"
                                                   type="text">
                                            <p> {{ trans('language.name_en') }} </p>
                                            <input name="name_en" value="{{ $subCategory->name_en }}" class="form-control"
                                                   type="text">



                                            <p> {{ trans('language.sections') }} </p>
                                            <select name="section_id" class="form-control">

                                                @foreach($Categories as $Category)
                                                    <option
                                                        {{ $subCategory->section_id == $Category->id ? 'selected' : "" }}   value="{{ $Category->id }}">
                                                        {{ $Category->name }} </option>
                                                @endforeach

                                            </select>


                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn green"> {{ trans('language.edit') }} </button>
                                    <button type="button" data-dismiss="modal"
                                            class="btn btn-outline dark"> {{  trans('language.close') }} </button>

                                </div>
                                {!! Form::close() !!}
                            </div>



                                <tr>

                                    <td>{{ $loop->iteration }}</td>

                                    <td>{{ $subCategory->name_ar }}</td>
                                    <td>{{ $subCategory->name_en }}</td>

                                    <td>{{ $subCategory->Category->name }}</td>


                                    {{--  export as  --}}
                                    <td>

                                        <ul class="" style="list-style: none">


                                            <li style="display: inline">
                                                {{-- responsive{{$region->id}} --}}
                                                <a title="تعديل بيانات المستخدم" class="btn btn-success"
                                                   data-toggle="modal"
                                                   href="#Editresponsive{{$subCategory->id}}">
                                                    <i class="fa fa-edit"></i> {{ trans('language.edit') }}  </a>
                                            </li>


                                            <li style="display: inline">
                                                {{--  DELETE BTN BTN   --}}
                                                <button data-id="" data-toggle="modal"
                                                        data-target="#delete_modal{{ $subCategory->id }}"
                                                        class="btn btn-danger">
                                                    <i class="fa fa-remove"></i>
                                                    {{  trans('language.delete') }}
                                                </button>

                                                {{--  DELETE Form    --}}
                                                <div id="delete_modal{{ $subCategory->id }}"
                                                     class="modal fade delete_modal bgmodal">
                                                    <div class="modal-dialog modal-confirm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <div class="icon-box">
                                                                    <i class="material-icons">&#xE5CD;</i>
                                                                </div>
                                                                <h4 class="modal-title">{{ trans('language.delete_danger') }}</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p> {{ trans('language.delete_aleart') }} ( <span
                                                                        style="color: #f15e5e">  {{$subCategory->name}} </span>
                                                                    ) ؟ </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                {!! Form::open(['route'=>["subcategories.destroy" , $subCategory->id ], 'class'=>"" ]) !!}
                                                                {!! method_field('DELETE') !!}
                                                                <button type="button" class="btn btn-info"
                                                                        data-dismiss="modal">{{ trans('language.close') }}</button>


                                                                <button type="submit"
                                                                        class="btn btn-danger">{{ trans('language.delete') }}</button>
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                    </td>

                                </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <!-- responsive -->
        <div id="Addresponsive" class="modal fade" tabindex="-1" data-width="760">
            {!! Form::open( array('route' => array('subcategories.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'AddForm'))  !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4> {{ trans('language.add') }} {{ trans('language.sections') }}</h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">


                        <p> {{ trans('language.name_ar') }}  </p>
                        <input name="name_ar" value="" class="form-control" type="text">
                        <p>{{ trans('language.name_en') }}</p>
                        <input name="name_en" value="" class="form-control" type="text">


                        <p> {{ trans('language.sections') }} </p>
                        <select name="section_id" class="form-control">

                            @foreach($Categories as $Category)
                                <option value="{{ $Category->id }}">
                                    {{ $Category->name }}</option>
                            @endforeach

                        </select>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green"> {{ trans('language.add') }}</button>
                    <button type="button" data-dismiss="modal"
                            class="btn btn-outline dark"> {{ trans('language.close') }}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- stackable -->

        </div>
    </div>


    <!-- END CONTENT BODY -->

@endsection
