


 
{{--  header part will be here  --}}
@include('AdminPanel.Partials.header')

{{--  navigation part will be here  --}}
@include('AdminPanel.Partials.navigation')


<div class="col-md-12">

    <div class="col-md-2">
        {{-- sidebar part will be here  --}}
        @include('AdminPanel.Partials.sidebar')

    </div>
    <div class="col-md-10">


        {{-- will Desing conent here ( Wrapper ) --}}
        @yield('content')
        @include('AdminPanel.Partials.message')

    </div>


</div>



{{-- // footer part will be here  --}}
@include('AdminPanel.Partials.footer')

@include('AdminPanel.Partials.modals')
