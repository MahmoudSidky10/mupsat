 
    
@extends('AdminPanel.app')

@section('page_title',  trans('language.dashboard') )

@section('content')


<h1 style="padding-top: 60px; margin-right: 20px;color: #fff" class="page-title">  
<small style="color: #FFF"></small>
</h1>





<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
                    <br>
                    <br>
                     
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="  table table-striped table-bordered table-hover dt-responsive" style="text-align: center;" width="100%" id="myTable">
                    <thead>
                        <tr>
                           
                            
                            <th class="all"> #</th>
                              
                            <th class="all">  أسم المنتج  </th>
                            <th class="all">  اسم العميل  </th>
                            <th class="all">  اسم الاسره  </th>
                            <th class="all">  السعر  </th>
                            <th class="all">  تفاصيل المنتج  </th>
                            <th class="all">  {{ trans('language.created_at') }} </th>
                            
                             

                            <th class="all">  {{ trans('language.settings') }} </th>

                        </tr>

                                
                            </thead>
                            <tbody>
                                
                                @foreach($favourites as $favourite)
                                @php
                                    $name_ar  = " $favourite->name_ar ";
                                @endphp
                                
                                <tr>
                                    
                                    <td>{{ $loop->iteration }}</td>
                                   
                                    <td>{{ $favourite->product->name }}</td>
                                    <td>{{ $favourite->user->name }}</td>
                                    <td>{{ $favourite->product->user->name }}</td>
                                    <td>{{ $favourite->product->price }}</td>
                                    <td>{{ $favourite->product->details_ar }}</td>
                                    <td>{{ $favourite->created_at->diffForHumans() }}</td>
                                     
                                     

                                   
                                     
                                    
                                  
                                    
                                    {{--  export as  --}}
                                <td>
                                      
                                            <ul class="" style="list-style: none">

                                                 
                                               
                                                  

                                                        

                                          
                                    <li style="display: inline"> 
                                                    {{--  DELETE BTN BTN   --}}
                                <button data-id="" data-toggle="modal"  data-target="#delete_modal{{ $favourite->id }}"
                                class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                                {{  trans('language.delete') }}
                                </button>

                                {{--  DELETE Form    --}}
                                <div id="delete_modal{{ $favourite->id }}" class="modal fade delete_modal bgmodal">
                                    <div class="modal-dialog modal-confirm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="icon-box">
                                                    <i class="material-icons">&#xE5CD;</i>
                                                </div>
                                                <h4 class="modal-title">{{ trans('language.delete_danger') }}</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <p> {{ trans('language.delete_aleart') }} (  <span style="color: #f15e5e">  {{$favourite->name_ar}} </span> ) ! </p>
                                            </div>
                                            <div class="modal-footer">
                                                 
                                                <form action='{{  url("deletefavourite/$favourite->id") }}' >
                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{ trans('language.close') }}</button>
                                                
                                                
                                                <button  type="submit" class="btn btn-danger">{{ trans('language.delete') }}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                </li>
                                                </ul>
                                           
                                    </td>

                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                  
                
            </div>
            
            
            
            <!-- END CONTENT BODY -->




@endsection