@php
    $lang = LaravelLocalization::getCurrentLocale();
    App::setLocale($lang);
    $citys = \App\City::all();
@endphp
@extends('AdminPanel.app')
@section('page_title',  trans('language.dashboard') )
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px; margin-right: 20px;color: #fff" class="page-title">
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase"> {{ trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal"
                           href="#Addresponsive"> {{ trans('language.add') }}
                        </a>
                        <a class="btn  btn-outline blue" data-toggle="modal"
                           href="#Messageresponsive"> {{ trans('language.sendMsg') }}
                        </a>

                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table style="" class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <th class="all"> #</th>
                            <th class="all"> {{   trans('language.image') }}</th>
                            <th class="all">  {{   trans('language.name') }} </th>
                            <th class="all"> {{   trans('language.email') }} </th>


                            <th class="all">  {{   trans('language.phone') }} </th>


                            <th class="all"> {{   trans('language.status') }}</th>

                            <th class="all"> {{   trans('language.mobile_verified') }}</th>
                            <th class="all"> {{   trans('language.settings') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            @php
                                $name  = " $user->first_name " . " $user->last_name ";
                            @endphp
                            {{-- Edit Modal --}}
                            <div id="Editresponsive{{$user->id}}" class="modal fade" tabindex="-1" data-width="760">
                                {!! Form::open( array('route' => array('users.update',$user->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'editform'))  !!}
                                {!! method_field('put') !!}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p> {{  trans('language.first_name') }} </p>
                                            <input name="name" value="{{ $user->first_name }}"
                                                   class="form-control" type="text">
                                            <p> {{  trans('language.last_name') }} </p>
                                            <input name="name" value="{{ $user->last_name }}"
                                                   class="form-control" type="text">

                                            <p> {{  trans('language.email') }} </p>
                                            <input name="email" value="{{ $user->email }}" class="form-control"
                                                   type="text">
                                            <p> {{  trans('language.phone') }} </p>
                                            <input name="mobile" required="required" value="{{ $user->mobile }}"
                                                   class="form-control " type="text">
                                            <p> {{ trans('language.city') }} </p>
                                            <select name="city_id" class="form-control">
                                                @foreach($citys as $city)

                                                    <option
                                                        {{ $user->city_id  == $city->id ? 'selected' :  ''  }}  value="{{ $city->id }}"> {{ $city->name  }} </option>

                                                @endforeach
                                            </select>


                                            <div class="col-xs-12 label">
                                                <label for="image">{{__('language.image')}}</label>
                                            </div>
                                            <div class="input">
                                                <img id="upload" class="upload" width="100" height="100"

                                                     @if($user->image)
                                                     src="{{$user->image}}"
                                                     @else
                                                     src="{{ asset('AdminPanelAssetFiles/default_user.jpg') }}">
                                                @endif

                                            </div>
                                            <input class="image" name="image" type="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn green"> {{  trans('language.edit') }}</button>
                                    <button type="button" data-dismiss="modal"
                                            class="btn btn-outline dark"> {{  trans('language.close') }}
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <tr>
                                <td class="centerCollom">{{ $loop->iteration }}</td>
                                <td>
                                    <img style="margin-top: 40px; border-radius: 10px !important" width="100px"
                                         height="100px" data-toggle="modal"
                                         data-target=".image_modal"
                                         class="view_image img-circle"
                                         @if($user->image)
                                         src="{{$user->image}}"
                                         @else
                                         src="{{ asset('AdminPanelAssetFiles/default_user.jpg') }}">
                                    @endif
                                </td>
                                <td class="centerCollom">
                                    @if( $name!= " " )
                                        {{ $name }}
                                    @else
                                        ---------
                                    @endif
                                </td>
                                <td class="centerCollom">
                                    @if( $user->email != "" )
                                        {{ $user->email }}
                                    @else
                                        ---------
                                    @endif
                                </td>


                                <td class="centerCollom" style="direction: ltr;"> {{ $user->mobile }}</td>


                                <td class="centerCollom">
                                    @if( $user->status == "active" )
                                        <span class="label label-primary"> {{ trans('language.avtived') }}  </span>
                                    @elseif($user->status == "not_active"  )
                                        <span class="label label-primary"> {{ trans('language.not_actived') }}  </span>
                                    @endif


                                </td>

                                <td class="centerCollom" style="padding-top: 10px">

                                    @if( $user->mobile_verified)
                                        <span
                                            class="label label-success"> {{ trans('language.mobile_verified_yes') }} </span>
                                    @else
                                        <span
                                            class="label label-danger"> {{ trans('language.mobile_verified_no') }} </span>
                                    @endif


                                </td>
                                {{--  export as  --}}
                                <td>
                                    <ul class="btnsList" style="padding-left: 0px!important;padding-right: 20px;">
                                        @if($user->status == "not_active")
                                            <li>
                                                <a href="{{ aurl("/active_Tech/$user->id") }}"
                                                   class="btn btn-primary"> {{ trans('language.active') }} </a>

                                            </li>
                                        @else
                                            <li>
                                                <a href="{{ aurl("/Not_active_Tech/$user->id") }}"
                                                   class="btn btn-info"> {{ trans('language.deActive') }}  </a>

                                            </li>
                                        @endif

                                        <li>
                                            <a href="{{ aurl("/userOrder/$user->id") }}"
                                               class="btn btn-primary"> {{ trans('language.myorder') }} </a>
                                        </li>
                                        <li>
                                            <a title="ارسال رساله للمستخدم" class="btn btn-info" data-toggle="modal"
                                               href="#Messageresponsive{{$user->id}}">
                                                <i style="" class="fa fa-envelope"></i>
                                                {{ trans('language.notification') }}
                                            </a>
                                            <!-- responsive -->
                                            <div id="Messageresponsive{{$user->id}}" class="modal fade" tabindex="-1"
                                                 data-width="760">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true"></button>
                                                    <h4 class="modal-title ">
                                                        [ <span
                                                            style="color: #32c5d2"> {{ $name }}</span> ]
                                                    </h4>
                                                </div>
                                                <form class="" method="get"
                                                      action="{{aurl("/notificationMessage/$user->id")}}">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4> {{ trans('language.notification') }} </h4>
                                                                {{--  old option to send msg in simple textarea ... --}}
                                                                <textarea class="form-control" name="messgae"
                                                                          style="  width: 714px; height: 149px;"
                                                                          rows="3"></textarea>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal"
                                                                    class="btn btn-outline dark">{{ trans('language.close') }}
                                                            </button>
                                                            <button type="submit"
                                                                    class="btn btn-outline dark"> {{ trans('language.send') }}
                                                        </div>
                                                        <!-- End form -->
                                                    </div>
                                                </form>
                                                <!-- stackable -->
                                            </div>
                                            <!-- stackable -->
                                        </li>

                                        <li>
                                            <a title="ارسال رساله للمستخدم" class="btn btn-success" data-toggle="modal"
                                               href="#userMessage{{$user->id}}">
                                                <i style="" class="fa fa-envelope"></i>
                                                {{ trans('language.sendMsg') }}
                                            </a>
                                            <!-- responsive -->
                                            <div id="userMessage{{$user->id}}" class="modal fade" tabindex="-1"
                                                 data-width="760">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true"></button>
                                                    <h4 class="modal-title ">
                                                        {{ trans('language.showsendMsg') }} [ <span
                                                            style="color: #32c5d2"> {{ $name }}</span> ]
                                                    </h4>
                                                </div>
                                                <form class="" method="get" action="{{aurl("/userMessage/$user->id")}}">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4> {{ trans('language.sendMsg') }} </h4>
                                                                {{--  old option to send msg in simple textarea ... --}}
                                                                <textarea class="form-control" name="messgae"
                                                                          style="  width: 714px; height: 149px;"
                                                                          rows="3"></textarea>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal"
                                                                    class="btn btn-outline dark">{{ trans('language.close') }}
                                                            </button>
                                                            <button type="submit"
                                                                    class="btn btn-outline dark"> {{ trans('language.send') }}
                                                        </div>
                                                        <!-- End form -->
                                                    </div>
                                                </form>
                                                <!-- stackable -->
                                            </div>
                                            <!-- stackable -->
                                        </li>
                                        <li>
                                            {{-- responsive{{$region->id}} --}}
                                            <a title="تعديل بيانات المستخدم" class="btn btn-success" data-toggle="modal"
                                               href="#Editresponsive{{$user->id}}">
                                                <i class="fa fa-edit"></i> {{ trans('language.edit') }} </a>
                                        </li>
                                        <li>
                                            {{--  DELETE BTN BTN   --}}
                                            <button data-id="" data-toggle="modal"
                                                    data-target="#delete_modal{{ $user->id }}"
                                                    class="btn btn-danger">
                                                <i class="fa fa-remove"></i>
                                                {{ trans('language.delete') }}
                                            </button>
                                            {{--  DELETE Form    --}}
                                            <div id="delete_modal{{ $user->id }}"
                                                 class="modal fade delete_modal bgmodal">
                                                <div class="modal-dialog modal-confirm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <div class="icon-box">
                                                                <i class="material-icons">&#xE5CD;</i>
                                                            </div>
                                                            <h4 class="modal-title">  {{ trans('language.delete_danger') }} </h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">&times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>  {{ trans('language.delete_aleart') }}

                                                                @if($name != " " )
                                                                    ( <span
                                                                        style="color: #f15e5e">  {{$name}} </span>
                                                                    ) ؟
                                                                @endif

                                                            </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            {!! Form::open(['route'=>["users.destroy" , $user->id ], 'class'=>"" ]) !!}
                                                            {!! method_field('DELETE') !!}
                                                            <button type="button" class="btn btn-info"
                                                                    data-dismiss="modal">{{  trans('language.close') }}
                                                            </button>
                                                            <button type="submit"
                                                                    class="btn btn-danger">{{  trans('language.delete') }}</button>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $users->links()  }}
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <!-- responsive -->
        <div id="Addresponsive" class="modal fade" tabindex="-1" data-width="760">
            {!! Form::open( array('route' => array('users.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>'AddForm'))  !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p> {{  trans('language.first_name') }}</p>
                        <input name="first_name" required="required" class="form-control" type="text">
                        <p> {{  trans('language.last_name') }}</p>
                        <input name="last_name" required="required" class="form-control" type="text">

                        <p> {{  trans('language.email') }}</p>
                        <input name="email" required="required" class="form-control" type="text">
                        <p> {{  trans('language.phone') }} </p>
                        <input required type="number" name="mobile" id="mobNum" pattern="^05[0-9]{8}" maxlength="10"
                               class="form-control input-text" value="" data-toggle="tooltip"
                               placeholder="{{__('language.phoneValidation')}} ">
                        <p> {{ trans('language.city') }} </p>
                        <select name="city_id" class="form-control">
                            @foreach($citys as $city)

                                <option value="{{ $city->id }}"> {{ $city->name }} </option>

                            @endforeach
                        </select>

                        <p>{{  trans('language.image') }} </p>
                        <input class="image form-control" name="image" type="file">
                        <p> {{  trans('language.password') }} </p>
                        <input name="password" required="required" class="form-control" type="password">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green"> {{  trans('language.add') }}</button>
                <button type="button" data-dismiss="modal"
                        class="btn btn-outline dark"> {{  trans('language.close') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- stackable -->
    </div>
    <div id="Messageresponsive" class="modal fade" tabindex="-1"
         data-width="760">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true"></button>
            <h4 class="modal-title ">
                {{ trans('') }}
            </h4>
        </div>
        <form class="" method="get" action="{{aurl("/UsersnotificationMessage")}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4> {{ trans('language.sendMsg') }} </h4>
                        {{--  old option to send msg in simple textarea ... --}}
                        <textarea class="form-control" name="messgae"
                                  style="  width: 714px; height: 149px;"
                                  rows="3"></textarea>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal"
                            class="btn btn-outline dark">{{ trans('language.close') }}
                    </button>
                    <button type="submit" class="btn btn-outline dark"> {{ trans('language.send') }}
                </div>
                <!-- End form -->
            </div>
        </form>
        <!-- stackable -->
    </div>
    <!-- stackable -->
    <!-- END CONTENT BODY -->
@endsection
