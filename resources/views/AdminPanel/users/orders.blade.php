 
@extends('AdminPanel.app')

@section('page_title',  trans('language.dashboard') )

@section('content')


   

    <!-- BEGIN CONTENT BODY -->


     <!-- BEGIN PAGE TITLE-->
                        <h1 style="padding-top: 60px;color: #FFF" class="page-title">  {{ trans('language.show') }} 
                            
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->

                                                    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }} </span>
                        <br>
                         
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <td>م</td>
                           
                          
                            <td>{{ trans('language.user_name') }} </td>
                           <td> {{ trans('اسم الاسره ') }}</td>
                           <td> اسم المنتج </td>
                           <td> سعر  المنتج </td>
                           <td>  قسم المنتج </td>
                           <td>{{ trans('language.status') }} </td>
                           <td>{{ trans('language.order_time') }} </td>
                             

                            

                        </tr>
                        </thead>
                        <tbody>

                        @if($orders->count())

                            @foreach($orders as $order)
 

                               <tr>
                                   <td>{{ $loop->iteration }}</td>

                                   <td>{{ $order->client->name }}</td>
                                   <td>{{ $order->user->name  /* Family */  }}</td>
                                   <td>{{ $order->product->name_ar }}</td>
                                   <td>{{ $order->product->price }}</td>
                                   <td>{{ $order->product->category->name_ar }}</td>
                                   <td>{{ $order->status }}</td>
                                   <td>{{ $order->created_at->diffForHumans() }}</td>
                                    
                                    
                                    
                               </tr>

                            @endforeach


                        @else

                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('language.no_data') }}</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>




@endsection