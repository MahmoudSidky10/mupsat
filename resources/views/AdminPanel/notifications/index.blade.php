   @php



       
             $admin = Auth::user();

    

        $lang = LaravelLocalization::getCurrentLocale(); 

      App::setLocale($lang);

    @endphp
    
@extends('AdminPanel.app')


@section('page_title',  trans('language.dashboard') )

@section('content')

    <div class="portlet box blue">

        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bell"></i>
                {{__('language.notifications')}}
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
                <a href="javascript:;" class="remove"> </a>
            </div>
        </div>
        <div class="portlet-body">

            <div class="portlet light bordered">
                <div class="portlet-body">


                    <ul class="nav nav-tabs">

                       

                        <li>
                            <a href="#clients_tab" data-toggle="tab"> {{__('language.clients')}} </a>
                        </li>
 

                    </ul>
                    <div class="tab-content">

                       

                        <div class="tab-pane fade active in" id="clients_tab">

                            <label class="mt-checkbox mt-checkbox-outline">  {{ trans('language.all') }}
                                    <input class="selectall" type="checkbox"  name="technician[]">
                                    <span></span>
                                </label>


                            <form method="post" action="{{aurl("notifications")}}"
                                  enctype="multipart/form-data">
                            @csrf

                            


                            <div class="mt-checkbox-list">

                                

                                 
                             @foreach($users as $user) 

                                <label class="mt-checkbox mt-checkbox-outline">

                                  @if($user->first_name != null )
                                  
                                    {{ $user->first_name }}  {{ $user->last_name }} 
                                  @else

                                  {{ $user->mobile }}

                                  @endif
                                  
                                    <input class="individual" type="checkbox" value="{{ $user->id }}" name="technician[]">
                                    <span></span>
                                </label>


                                 

                            @endforeach

                                                         
                            </div>


 

                            <div  class="">
                                
                                <label> {{ trans('language.title_msg') }} </label>
                                <input type="text" class="form-control" name="title">

                            </div>

                            <br>


                            <div  class="">
                                
                                <p> {{ trans('language.body_msg') }} </p>
                                <textarea style="height: 200px;width: 100%" name="body" >  </textarea>

                            </div>

                            <div class="modal-footer">
                                                             
                                 <button type="submit" class="btn btn-outline BLUE"> {{ trans('language.send') }}

                            </div>
                                
                            </form>
                        </div>

                        

                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

@section('js')
    <script>
        $("#dash").removeClass("active");
        $(".notifications").addClass("open active").css("display", "block");



    </script>
@endsection
@section('css')
    <style>
        .actions .btn-warning, .updated_at {
            display: none !important;
        }
    </style>
@endsection