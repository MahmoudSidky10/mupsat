


@php





    $lang = LaravelLocalization::getCurrentLocale();

  App::setLocale($lang);


@endphp

@extends('AdminPanel.app')


@section('page_title',  trans('language.dashboard') )

@section('content')


   <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px;color: #FFF" class="page-title"> {{  trans('language.show') }}
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->


            <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal" href="#responsive"> {{ trans('language.add') }}  </a>

                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table  class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <td> # </td>
                            <td> {{ trans('language.name') }} </td>
                            <td> {{ trans('العمله ') }} </td>
                             
 
                            
                             
                            <td> {{ trans('language.settings') }} </td>
                             

                        </tr>
                        </thead>
                        <tbody>

                        @if($countries->count())

                            @foreach($countries as $countrie)

                                {{-- Edit Modal --}}
                                <div id="responsive{{$countrie->id}}" class="modal fade" tabindex="-1" data-width="760">
                                    {!! Form::open( array('route' => array('countries.update',$countrie->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>''))  !!}
                                    {!! method_field('put') !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title"> ا{{ trans('language.add') }} </h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="row">
                                            <div class="col-md-12">

                                              


                                                <p> {{ trans('language.name_ar') }}    </p>
                                                <input oninvalid="setCustomValidity('هذا الحقل يجب ادخاله')" name="name_ar" required="required" value="{{$countrie->name_ar }}" class="form-control" type="text">


                                                <p> {{ trans('العمله') }}    </p>
                                                <input oninvalid="setCustomValidity('هذا الحقل يجب ادخاله')" name="currency_ar" required="required" value="{{$countrie->currency_ar }}" class="form-control" type="text">


                                                

                      

                                            </div>

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn green"> {{ trans('language.save') }} </button>
                                        <button type="button" data-dismiss="modal" class="btn btn-outline dark"> {{ trans('language.close') }} </button>

                                    </div>

                                    {!! Form::close() !!}
                                </div>


                               <tr>
                                   <td>{{ $loop->iteration }}</td>
                                   <td>{{ $countrie->name }}</td>
                                   <td>{{ $countrie->currency_ar }}</td>
                                  
                                  
                                   
                                     
                                    <td>
                                        
                                            <ul style="list-style: none">

                                                <li style="display: inline;">
                                                    <a class="btn btn-info" data-toggle="modal" href="#responsive{{$countrie->id}}">
                                                        <i
                                                                class="fa fa-print"></i> {{  trans('language.edit') }} 

                                                    </a>

                                                </li>
                                                
                                                  <li style="display: inline;">

                                <button data-id="" data-toggle="modal"  data-target="#delete_modal{{ $countrie->id }}"
                                class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                                {{ trans('language.delete') }} 
                                </button>


                                {{--  DELETE Form    --}}
                                <div id="delete_modal{{ $countrie->id }}" class="modal fade delete_modal bgmodal">
                                    <div class="modal-dialog modal-confirm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="icon-box">
                                                    <i class="material-icons">&#xE5CD;</i>
                                                </div>
                                                <h4 class="modal-title"> {{ trans('language.delete_danger') }}؟</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <p> {{ trans('language.delete_aleart') }}  (  <span style="color: #f15e5e">  {{$countrie->name_ar}} </span> ) ؟  </p>
                                            </div>
                                            <div class="modal-footer">
                                                {!! Form::open(['route'=>["countries.destroy" , $countrie->id ], 'class'=>"" ]) !!}
                                                {!! method_field('DELETE') !!}
                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{ trans('language.close') }} </button>
                                                
                                                
                                                <button  type="submit" class="btn btn-danger">{{ trans('language.delete') }} </button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                                   

                                                </li>
                                            </ul>
                                        
                                    </td>
                               </tr>

                            @endforeach


                        @else

                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('no_data') }}</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>


    {{-- Add modal   --}}
    <!-- responsive -->

    <div id="responsive" class="modal fade" tabindex="-1" data-width="760">
        {!! Form::open( array('route' => array('countries.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>''))  !!}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> {{ trans('language.add') }}</h4>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">

                    


                    <p> {{ trans('language.name_ar') }}  </p>
                    <input oninvalid="setCustomValidity('هذا الحقل يجب ادخاله')" name="name_ar" required="required" class="form-control" type="text">              

                          <p> {{ trans('العمله ') }}  </p>
                    <input  name="currency_ar" required="required" class="form-control" type="text">


                    




                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn green"> {{ trans('language.add') }} </button>
            <button type="button" data-dismiss="modal" class="btn btn-outline dark"> {{ trans('language.close') }} </button>

        </div>

        {!! Form::close() !!}
    </div>
    <!-- stackable -->

                       


@endsection
