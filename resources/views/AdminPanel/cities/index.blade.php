@php



 

    $lang = LaravelLocalization::getCurrentLocale();

  App::setLocale($lang);

  $countries = \App\Country::all();

@endphp


@extends('AdminPanel.app')


@section('page_title',  trans('language.dashboard') )

@section('content')


    <!-- BEGIN CONTENT BODY -->


    <!-- BEGIN PAGE TITLE-->
    <h1 style="padding-top: 60px;color: #FFF" class="page-title"> {{  trans('language.show') }}
        <small style="color: #FFF"></small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{  trans('language.show') }}</span>
                        <br>
                        <br>
                        <a class="btn btn-outline green" data-toggle="modal"
                           href="#responsive">{{  trans('language.add') }}   </a>

                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td> {{  trans('language.name_ar') }} </td>
                            <td> {{  trans('language.name_en') }}</td>
                            <td> {{  trans('language.country') }} </td>
                          
 


                            <td></td>


                        </tr>
                        </thead>
                        <tbody>

                        @if($cities->count())

                            @foreach($cities as $citie)

                                {{-- Edit Modal --}}
                                <div id="responsive{{$citie->id}}" class="modal fade" tabindex="-1" data-width="760">
                                    {!! Form::open( array('route' => array('cities.update',$citie->id ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>''))  !!}
                                    {!! method_field('put') !!}


                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true"></button>
                                        <h4 class="modal-title"> {{ trans('language.edit_city') }}</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="row">
                                            <div class="col-md-12">

                                                <h4> {{ trans('language.show') }} </h4>


                                                <p> {{ trans('language.name_ar') }}  </p>
                                                <input name="name_ar" required="required" value="{{$citie->name_ar }}"
                                                       class="form-control" type="text">


                                                <p> {{ trans('language.name_en') }} </p>
                                                <input name="name_en" required="required" value="{{$citie->name_en }}"
                                                       class="form-control" type="text">


                                                       <p> {{ trans('language.countries') }} </p>
                                                <select name="country_id" class="form-control">

                                                    @foreach($countries as $countrie)
                                                        <option
                                                            {{ $citie->country_id == $countrie->id ? 'selected' : "" }}  value="{{ $countrie->id }}"> {{ $countrie->name_ar }}
                                                            / {{ $countrie->name }}</option>
                                                    @endforeach

                                                </select>

                                                 


                                                


                                            </div>

                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn green"> {{ trans('language.edit') }} </button>
                                        <button type="button" data-dismiss="modal"
                                                class="btn btn-outline dark"> {{  trans('language.close') }} </button>

                                    </div>

                                    {!! Form::close() !!}
                                </div>


                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $citie->name_ar }}</td>
                                    <td>{{ $citie->name_en}}</td>
                                    <td>{{ $citie->country->name }}</td>
                      
                                


                                    <td>

                                        <ul style="list-style: none">

                                            <li style="display: inline;">
                                                <a class="btn btn-info" data-toggle="modal"
                                                   href="#responsive{{$citie->id}}">
                                                    <i
                                                            class="fa fa-print"></i> {{ trans('language.edit') }}

                                                </a>

                                            </li>

                                            <li style="display: inline;">

                                                <button data-id="" data-toggle="modal"
                                                        data-target="#delete_modal{{ $citie->id }}"
                                                        class="btn btn-danger">
                                                    <i class="fa fa-remove"></i>
                                                    {{ trans('language.delete') }}
                                                </button>

                                                {{--  DELETE Form    --}}
                                                <div id="delete_modal{{ $citie->id }}"
                                                     class="modal fade delete_modal bgmodal">
                                                    <div class="modal-dialog modal-confirm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <div class="icon-box">
                                                                    <i class="material-icons">&#xE5CD;</i>
                                                                </div>
                                                                <h4 class="modal-title"> {{ trans('language.delete_danger') }}</h4>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                            </div>
                                                            
                                                            <div class="modal-footer">
                                                                {!! Form::open(['route'=>["cities.destroy" , $citie->id ], 'class'=>"" ]) !!}
                                                                {!! method_field('DELETE') !!}
                                                                <button type="button" class="btn btn-info"
                                                                        data-dismiss="modal">{{ trans('language.close') }} </button>


                                                                <button type="submit"
                                                                        class="btn btn-danger">{{ trans('language.delete') }} </button>
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </li>
                                        </ul>

                                    </td>
                                </tr>

                            @endforeach


                        @else

                            {{-- show than no data yet for this User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('no_data') }}</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>


    {{-- Add modal   --}}
    <!-- responsive -->

    <div id="responsive" class="modal fade" tabindex="-1" data-width="760">
        {!! Form::open( array('route' => array('cities.store' ) , 'class'=> '' , 'files'=>true , 'method'=>'POST' , 'id'=>''))  !!}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">

                    <h4> {{ trans('language.show') }} </h4>


                    <p> {{ trans('language.name_ar') }}  </p>
                    <input name="name_ar" required="required" class="form-control" type="text">


                    <p>   {{ trans('language.name_en') }} </p>
                    <input name="name_en" required="required" class="form-control" type="text">

                    <p> {{ trans('language.countries') }} </p>
                    <select name="country_id" class="form-control">

                        @foreach($countries as $countrie)
                            <option value="{{ $countrie->id }}">
                                {{ $countrie->name }}</option>
                        @endforeach

                    </select>

                
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn green">  {{ trans('language.save') }}</button>
            <button type="button" data-dismiss="modal"
                    class="btn btn-outline dark"> {{ trans('language.close') }}</button>

        </div>

        {!! Form::close() !!}
    </div>
    <!-- stackable -->




@endsection
