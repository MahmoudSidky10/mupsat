@php

$lang = LaravelLocalization::getCurrentLocale();
      App::setLocale($lang);

      @endphp 
    
@extends('AdminPanel.app')

@section('page_title',  trans('language.dashboard') )

@section('content')


   

    <!-- BEGIN CONTENT BODY -->


     <!-- BEGIN PAGE TITLE-->
                        <h1 style="padding-top: 60px;color: #FFF" class="page-title">  {{ trans('language.show') }}   </h1>
                       

                                                    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">{{ trans('language.show') }} </span>
                        <br>
                        <br>

                        <a class="btn btn-outline red" data-toggle="modal"
                           href="{{ url('/deletecontacts') }}"> حذف الكل 
                       </a>
                         
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <table class="  table table-striped table-bordered table-hover dt-responsive"
                           style="text-align: center;" width="100%" id="myTable">
                        <thead>
                        <tr>
                            <td>#</td>
                            
                            <td> {{ trans('language.name') }}    </td>
                            <td> {{ trans('language.email') }}    </td>
                            <td> {{ trans('language.title') }}    </td>
                            <td> {{ trans('language.sendMsg') }}    </td>
                            <td> {{ trans('language.send_time') }}    </td>
                            <td> {{ trans('language.settings') }}  </td>
                             

                        </tr>
                        </thead>
                        <tbody>

                        @if($contacts->count())

                            @foreach($contacts as $contact)



                               <tr>
                                   <td>{{ $loop->iteration }}</td>
                                   
                                   <td> {{ $contact->name }}</td>
                                   <td> {{ $contact->email }}</td>
                                   <td> {{ $contact->title }}</td>
                                   <td> {{ $contact->message }}</td>  
                                   <td  >{{$contact->created_at->diffForHumans()}}</td>
                                    <td>
                                        
                                            <ul style="list-style: none">
 
                                                  <li style="display: inline;">

                                <button data-id="" data-toggle="modal"  data-target="#delete_modal{{ $contact->id }}"
                                class="btn btn-danger">
                                <i class="fa fa-remove"></i>
                                {{ trans('language.delete') }}  
                                </button>

                                {{--  DELETE Form    --}}
                                <div id="delete_modal{{ $contact->id }}" class="modal fade delete_modal bgmodal">
                                    <div class="modal-dialog modal-confirm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div class="icon-box">
                                                    <i class="material-icons">&#xE5CD;</i>
                                                </div>
                                                <h4 class="modal-title">{{ trans('language.delete_danger') }} </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{ trans('language.delete_aleart') }}  </p>
                                            </div>
                                            <div class="modal-footer">


                                                <form action="{{url("deletecontactsid/$contact->id")}}">
                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{ trans('language.close') }} </button>
                                                
                                                
                                                <button  type="submit" class="btn btn-danger">{{ trans('language.delete') }} </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                                   

                                                </li>
                                            </ul>
                                        
                                    </td>
                               </tr>

                            @endforeach


                        @else

                            {{-- show than no data yet for this suggestion->User .. --}}
                            <tr>
                                <td colspan="30"
                                    class="text-center">{{ trans('language.no_data') }}</td>
                            </tr>

                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>




@endsection