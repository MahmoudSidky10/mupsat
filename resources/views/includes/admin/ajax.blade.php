<script>
    $(".country_id").on('change', function () {
        $(".city_id").empty();
        $('.loader').fadeIn();
        $.post("{{url("get_cities")}}",
            {
                country_id: $(this).val()
            },
            function (data, status) {
                $.each(data, function () {
                    $.each(this, function (index, item) {
                        $("#city_id").prepend('' +
                            '<option selected value="' + item.id + '">' + item.name_ar + '</option>');
                    });
                });
                $('.loader').fadeOut();
            });
    });
</script>
