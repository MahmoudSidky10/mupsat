<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ركنى</title>
    <link rel="stylesheet" href="{{asset('assets/ar/site/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/ar/site/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/ar/site/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/ar/site/css/main.css')}}">
</head>