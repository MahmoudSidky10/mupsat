<footer>
    <div class="container">
        <div class="row">
            <div class="rights col-sm-6">
                <h4>جميع الحقوق محفوظة &copy; لتطبيق ركني 2018 &commat; 2019</h4>
            </div>

            <div class="politics col-sm-6">
               <div class="link">
                   <a href="{{url('politics')}}">سياسة الخصوصية</a>
                   <span></span>
               </div>
            </div>
        </div>
    </div>
</footer>
