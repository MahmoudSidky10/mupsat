<div class="footer-politics">
    <div class="container">
        <div class="row">
             <div class="rights col-lg-6 col-12">
                <h4>جميع الحقوق محفوظة &copy; لتطبيق ركني 2018 &commat; 2019</h4>
             </div> 
             <div class="follow-us col-lg-6 col-12">
                <span>تابعونــا</span>
                <div class="links col-lg-6 col-12">
                     <ul>
                          <li><a href="#"><i class="fab fa-google"></i></a></li>
                          <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                          <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                          <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    </ul>
                </div>
             </div>          
        </div>
    </div>
</div>