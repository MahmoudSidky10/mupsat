<?php


Route::get('/', 'AdminPanel\IndexController@homepage');
Route::post('/sitecontact', 'AdminPanel\IndexController@sitecontact');

if (!function_exists('aurl')) {
    function aurl($link)
    {
        if (substr($link, 0, 1) == '/') {
            return url(LaravelLocalization::setLocale() . $link);
        } else {
            return url(LaravelLocalization::setLocale() . '/' . $link);
        }
    }
}

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localization']], function () {


// Active Email :-
    Route::get('/verifyEmail/{id}', 'AdminPanel\IndexController@verifyEmail');
    Route::get('/resetPass/{id}', 'AdminPanel\IndexController@resetPass');

    Route::post('/logout', 'Auth\LoginController@logout');

// ### Admin will Play here ### ///
    Route::get('/admin', function () {


        if (Auth::check()) {
            return redirect('/dash');
        } else {
            return view('AdminPanel.auth.loginAuth');
        }


// retuen login page to enter dashboard .. it aviliable only for Admin


    })->name('startPage');

    Route::post('/loginAdmin', 'AdminPanel\IndexController@login');

    Route::view('/noAuth', 'AdminPanel.errors.notallowed');

    Route::post('/getSubCategories', 'AdminPanel\IndexController@getSubCategories');

});


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['auth', 'localization']], function () {


    Route::get('/dash', 'AdminPanel\IndexController@index')->name('dash');

    //
    route::resource('/users', 'AdminPanel\UserController');

    Route::get('active_Tech/{id}', 'AdminPanel\IndexController@active_Tech');
    Route::get('Not_active_Tech/{id}', 'AdminPanel\IndexController@Not_active_Tech');
    Route::get('userOrder/{id}', 'AdminPanel\IndexController@userOrder');
    Route::get('notificationMessage/{id}', 'AdminPanel\IndexController@notificationMessage');
    Route::get('UsersnotificationMessage', 'AdminPanel\IndexController@UsersnotificationMessage');
    Route::get('userMessage/{id}', 'AdminPanel\IndexController@userMessage');


    // notifications
    Route::resource('/notifications', 'AdminPanel\NotificationsController');


    route::resource('/sections', 'AdminPanel\CategoriesController');

    // subcategories
    route::resource('/subcategories', 'AdminPanel\SubCategoriesController');


    route::resource('/products', 'AdminPanel\ProductsController');


    route::resource('/orders', 'AdminPanel\OrderController');


    route::resource('/questions', 'AdminPanel\QuestionController');

    route::resource('/galleries', 'AdminPanel\GallerieController');

//         route::resource('/termes','AdminPanel\TermesController');

    route::resource('/sale_termes', 'AdminPanel\Sale_TermesController');


    route::resource('/soicalMedias', 'AdminPanel\SoicalMediaController');

    // cities CRUD :-
    Route::resource('/cities', 'AdminPanel\CityController');

    // areas
    Route::resource('/areas', 'AdminPanel\AreaController');


    // conversations
    Route::resource('/conversations', 'AdminPanel\ConversationsController');
    //openConversation
    Route::get('/openConversation/{id}', 'AdminPanel\ConversationsController@openConversation');

    // send new message
    Route::get('/saveMessage', 'AdminPanel\ConversationsController@saveMessage');


    // About Rokny:-
    Route::get('/aboutMapsut', 'AdminPanel\AboutMapsutController@index')->name('about');
    Route::put('/about/{id}', 'AdminPanel\AboutMapsutController@update')->name('about_update');

    route::get('/termes', 'AdminPanel\TermesController@index')->name('termes');
    Route::put('/termes/{id}', 'AdminPanel\TermesController@update')->name('termes_update');

    route::get('/termesSale', 'AdminPanel\Sale_TermesController@index')->name('termesSale');
    Route::put('/termesSale/{id}', 'AdminPanel\Sale_TermesController@update')->name('termesSale_update');


    // contacts
    Route::get('contacts', 'AdminPanel\IndexController@contacts');
    Route::get('deletecontacts', 'AdminPanel\IndexController@deletecontacts');
    Route::get('deletecontactsid/{id}', 'AdminPanel\IndexController@deletecontactsid');

    //getProducts of one family
    Route::get('/getProducts', 'AdminPanel\OrderController@getProducts');
    // storeOrder
    route::post('/storeOrder', 'AdminPanel\OrderController@storeOrder');
    route::any('/deleteorder/{id}', 'AdminPanel\OrderController@deleteorder');
    // order Product 

    route::any('/orderProducts/{id}', 'AdminPanel\OrderController@orderProducts');
    route::any('/orderBankInformation/{id}', 'AdminPanel\OrderController@orderBankInformation');


});

route::get('/sms', 'site\HomeController@sms');
