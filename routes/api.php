<?php


// ####  Not Auth #### //
Route::middleware(['lang', 'initRequest'])->group(function () {
    // Client Register :-
    Route::post('/clientRegister', 'api\ClientsController@register');
    // Client And Family Login
    Route::post('/login', 'api\shareController@login');


    // Forget Password
    Route::post('/forgetPassword', 'api\shareController@forgetPassword');
    Route::post('/checkPassCode', 'api\shareController@checkPassCode');
    Route::post('/updateUserPass', 'api\shareController@updateUserPass');
    Route::post('/resendUserPass', 'api\shareController@resendUserPass');

// tst sms


    // About App
    Route::post('/sms', 'api\shareController@sms');

    // termes App
    Route::post('/termsApp', 'api\shareController@termeApp');

    // Sale Termes
    Route::post('/saleTermsApp', 'api\shareController@Sale_termeApp');

    // show images of Gallery
    Route::post('/galleryImages', 'api\shareController@Gallery_images');

    // Show Social Media List
    Route::post('/socialMedia', 'api\shareController@SocialMedia');

    // list of Questions
    Route::post('/questions', 'api\shareController@Questions');

    // whatsAppNumber
    Route::post('/whatsAppNumber', 'api\shareController@whatsAppNumber');

    // activate client Account
    Route::post('/activateUser', 'api\shareController@activate');
    Route::post('/reSendActivateCode', 'api\shareController@resensActivateCode');



    // Main Page Show ..
    Route::post('/homePage', 'api\shareController@homePage');

    // bank Account information ..
    Route::post('/bankAccountInformation', 'api\shareController@bankAccountInformation');
    
    // store sale request
    Route::post('/bankSaleRequest', 'api\shareController@bankSaleRequest');


    // Contact Us Section
    Route::post('/userContact', 'api\shareController@user_contact');

    // product List  Show ..
    Route::post('/productList', 'api\shareController@productList');

    // specials Products list
    Route::post('/specialProductList', 'api\shareController@specialProductList');


    // show Details of product
    Route::post('/productDetails', 'api\shareController@product_details');


        // show cites lisyt
    Route::post('/cities', 'api\shareController@cities');
    Route::post('/countries', 'api\shareController@countries');


});


// ####  Not #### //
Route::middleware(['lang', 'initRequest', 'auth:api'])->group(function () {


    // make new order Request ..
    Route::post('/makeOrder', 'api\shareController@makeOrder'); // Cashe money 
     Route::post('/makeOrderViaBankTransfer', 'api\shareController@makeOrderViaBankTransfer');// Transform with bank 

    // get client order .
    Route::post('/clientOrders', 'api\shareController@client_orders');

    // order Details
    Route::post('/orderDetails', 'api\shareController@orderDetails');

    // get client Profile
    Route::post('/profile', 'api\ClientsController@profile');

    // update profile data
    Route::post('/updateProfile', 'api\ClientsController@updateProfile');

    // show user notifications
    Route::post('/userNotifications', 'api\shareController@user_notifications');
    Route::post('/deleteUserNotifications', 'api\shareController@deleteUserNotifications');



});

Route::middleware(['initRequest'])->group(function () {

    // Locations
    Route::post('/locations', 'api\shareController@Locations');

});
