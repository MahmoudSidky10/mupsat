<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
     public $timestamps = false;
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'currency_ar',
        'currency_en',
        'code_name',
        'code',
    ];

    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getCurrencyAttribute()
    {
        if (\request()->lang == "en")
            return $this->currency_en;
        else
            return $this->currency_ar;
    }

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['image'] = $this->image;
        $data['name'] = $this->name;
        $data['currency'] = $this->currency;
        $data['currency_ar'] = $this->currency_ar;
        $data['currency_en'] = $this->currency_en;
        $data['code_name'] = $this->code_name;
        $data['code'] = $this->code;
        $data['cities'] = $this->cities;
        return $data;
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }
}
