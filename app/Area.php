<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name_ar',
        'name_en',
        'country_id',
        'city_id',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        return $data;
    }

    public function getNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

   
     public function city()
    {
        return $this->belongsTo(City::class);
    }
}
