<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function politics()
    {
        return view('site.politics');
    }

    public  function home()
    {
        return view('site.home');
    }

    public  function sms()
    {

    }

}
