<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated()
    {
        if (auth()->check() and auth()->user()->user_type_id == 1) // admin
            return redirect('/admin/subscribes');
        return redirect('/');
    }

    protected function logout()
    {
        $url = redirect('/');
        if (auth()->check() and auth()->user()->user_type_id == 1) // admin
            $url = redirect('/admin/subscribes');
        Auth::logout();
        return $url;
    }
}
