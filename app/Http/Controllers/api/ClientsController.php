<?php

namespace App\Http\Controllers\api;

use App\Notification;
use App\User_notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Mail\VerifyMail;
use Mail;

class ClientsController extends Controller
{

    // register function
    public function register(Request $request)
    {
        $request->validate([
            'device_type' => 'required|string',
            'fire_base_token' => 'required|string',
            'mobile' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ]);
        $data = $request->all();

        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = "+966" . $str;
        } else {
            $data["mobile"] = "+966" . $data["mobile"];
        }
        // check if  mobile was exist ?
        $mobileCheck = User::where('mobile', $data["mobile"])->get();
        if ($mobileCheck->count() > 0) {
            return $this->apiResponse($request, trans('language.Existmobile'), null, false);
        }
        // if Not Existed ( Email or mobile ) ? create new Account ..
        $data['api_token'] = rand(1111, 9999) . time();
        $data['code'] = rand(1111, 9999);
        $data['password'] = bcrypt($request->password);
        $data['type'] = 'client';
        $user = User::create($data);
        $user = User::find($user->id);
        $this->smsActivationCodeHandler($user);
        return $this->apiResponse($request, trans('language.message'), $user, true);

    }

    public function smsActivationCodeHandler($user)
    {
        $mobile = $user->mobile;
//        $code = $result = substr($mobile, 0, 3);
//        $code = ltrim($code, '+');
//        if ($code != "966")
//            $mobile = "966" . $mobile;
        $str = "تطبيق مبسط لمواد البناء";
        $code = "رقم التفعيل هو : ";
        $message = $str . $code . $user->code;
        $this->sendSms($mobile, $message);
    }

    // get profile
    public function profile(Request $request)
    {
        $data = $request->user()->profile();
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    // updateProfile
    public function updateProfile(Request $request)
    {
        /*
        if ($request->email) {
            if ($request->email != $request->user()->email)
                $this->sendEmail($request->email);
            $request->request->remove('email');
        } */

        $data = $request->data;
        $user = $request->user();
        $user->update($data);
        return $this->apiResponse($request, trans('language.message'), $user->profile(), true);
    }

}
