<?php

namespace App\Http\Controllers\api;

use App\About;
use App\City;
use App\ContactWithApp;
use App\Country;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProducts;
use App\Product;
use App\ProductSubCategory;
use App\Question;
use App\saleBankRequest;
use App\SaleTerme;
use App\Section;
use App\SocialMedia;
use App\SubCategory;
use App\Terme;
use App\User;
use App\User_notification;
use Auth;
use Carbon;
use Illuminate\Http\Request;


class shareController extends Controller
{

    public function cities(Request $request)
    {
        $cities = City::orderBy("name_ar", "asc")->get();;
        return $this->apiResponse($request, trans('language.message'), $cities, true);
    }

    public function countries(Request $request)
    {
        $items = Country::orderBy("name_ar", "asc")->get();
        return $this->apiResponse($request, trans('language.message'), $items, true);
    }

    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string|max:255',
            'password' => 'required|string|max:255',
        ]);

        $mob = $request->mobile;
        $MobstartWith = substr($request->mobile, 0, 1);
        if($MobstartWith == 0)
        {
            $str = ltrim($mob, '0');
            $data["mobile"] = "+966" . $str;
        }else{
            $data["mobile"] = "+966" . $request->mobile;
        }

        if (Auth::attempt(['mobile' => $data["mobile"] , 'password' => $request->password])) {
            // The user is active, not suspended, and exists.
            $user = Auth::user();

            return $this->apiResponse($request, trans('language.message'), $user, true);
        } else {
            return $this->apiResponse($request, __('auth.failed'), null, false);
        }

    }// end login

    public function forgetPassword(Request $request)
    {
        $request->validate([
            'mobile' => 'required|string|max:255',
        ]);

        // check if email was exist ?
        $user = User::where('mobile', $request->mobile)->first();

        if ($user) {
            // yes And Send Email to reset his password ?
            // send sms to User To Reset his Email ...
            // Mail::to($user)->send(new resetPasswordMail($user));


            //$user->passCode = rand(1111, 9999);
            $user->passCode = "1234";
            $user->save();

            $data['passCode'] = $user->passCode;
            return $this->apiResponse($request, trans('language.message'), null, true);
        } else {
            // no and return that this Email Not Exist in db !!!
            return $this->apiResponse($request, __('language.not_Existemobile'), null, false);
        }


    }

    // checkPassCode
    public function checkPassCode(Request $request)
    {
        $request->validate([
            'passCode' => 'required|string|max:255',
        ]);

        // check if email was exist ?
        $user = User::where('mobile', $request->mobile)->first();
        if ($request->passCode != $user->passCode) {
            return $this->apiResponse($request, __('language.not_Matched'), null, false);
        } else {
            return $this->apiResponse($request, trans('language.message'), null, true);
        }

    }

    // updateUserPass
    public function updateUserPass(Request $request)
    {
        $request->validate([
            'password' => 'required|string|max:255',
            'mobile' => 'required|string|max:255',
            'passCode' => 'required|string|max:255',
        ]);

        // check if email was exist ?
        $user = User::where('mobile', $request->mobile)->where('passCode', $request->passCode)->first();
        if ($user) {
            $user->password = bcrypt($request->password);
            $user->save();
            return $this->apiResponse($request, trans('language.message'), null, true);

        } else {
            return $this->apiResponse($request, trans('language.failed'), null, false);
        }

    }

    // resendUserPass
    public function resendUserPass(Request $request)
    {
        // resend the code with sms ( passCode )
        $request->validate([
            'mobile' => 'required|string|max:255',
        ]);

        // check if email was exist ?
        $user = User::where('mobile', $request->mobile)->first();

        if ($user) {
            // yes And Send Email to reset his password ?
            // send sms to User To Reset his Email ...
            // todo ! Sms Code .
            $user->passCode = "1234";
            $user->save();

            $data['passCode'] = $user->passCode;
            return $this->apiResponse($request, trans('language.message'), $data, true);
        } else {
            // no and return that this Email Not Exist in db !!!
            return $this->apiResponse($request, __('language.not_Existemail'), null, false);
        }
    }


    // About fun
    public function aboutApp(Request $request)
    {

        $aboutApp = About::find(1);
        return $this->apiResponse($request, trans('language.message'), $aboutApp, true);
    }

    // termeApp fun
    public function termeApp(Request $request)
    {

        $termeApp = Terme::find(1);
        if ($termeApp) {
            return $this->apiResponse($request, trans('language.message'), $termeApp, true);
        } else {
            return $this->apiResponse($request, trans('language.message'), null, false);
        }

    }

    // Sale_termeApp

    public function Sale_termeApp(Request $request)
    {

        $Sale_termeApp = SaleTerme::find(1);
        if ($Sale_termeApp) {
            return $this->apiResponse($request, trans('language.message'), $Sale_termeApp, true);
        } else {
            return $this->apiResponse($request, trans('language.message'), null, false);
        }

    }

    // user_contact
    public function user_contact(Request $request)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'message' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'number' => 'required|string|max:255',
        ]);

        $items = $request->all();
        $ContactUs = ContactWithApp::create($items);
        return $this->apiResponse($request, trans('language.message'), $ContactUs, true);
    }


    // Gallery_images
    public function Gallery_images(Request $request)
    {
        $lang = $request->lang ;
        $images = Gallery::orderBy('id', 'desc')->where('lang',$lang)->where('status', 'active')->get();
        return $this->apiResponse($request, trans('language.message'), $images, true);
    }


    // Home page
    public function homePage(Request $request)
    {
        $Sections = Section::orderBy('sort', 'asc')->get();
        return $this->apiResponse($request, trans('language.message'), $Sections, true);

    }

    // SocialMedia
    public function SocialMedia(Request $request)
    {
        $SocialMedia = SocialMedia::orderBy('id', 'desc')->get();
        return $this->apiResponse($request, trans('language.message'), $SocialMedia, true);

    }


    //productList
    public function productList(Request $request)
    {
        $request->validate([
            'section_id' => 'required|string|max:255',
        ]);

        // Product List ..
        $products = Product::orderBy('id', 'desc')->where('section_id', $request->section_id)->get();
        return $this->apiResponse($request, trans('language.message'), $products, true);
    }

    // specialProductList
    public function specialProductList(Request $request)
    {

        // Product List ..
        $products = Product::orderBy('id', 'desc')->where('special', "1")->get();
        return $this->apiResponse($request, trans('language.message'), $products, true);
    }

    //product_details
    public function product_details(Request $request)
    {
        $request->validate([
            'product_id' => 'required|string|max:255',
        ]);

        $product = Product::orderBy('id', 'desc')->where('id', $request->product_id)->first();
        return $this->apiResponse($request, trans('language.message'), $product->productObj(), true);

    }

    // client_orders
    public function client_orders(Request $request)
    {
        $userid = $request->user()->id;
        if (!$userid)
            abort(404);

        $orders = Order::orderBy('id', 'desc')->where('user_id', $userid)->get();

        return $this->apiResponse($request, trans('language.message'), $orders, true);
    }

    // orderDetails
    public function orderDetails(Request $request)
    {

        $request->validate([
            'order_id' => 'required|string|max:255',
        ]);

        $userid = $request->user()->id;
        if (!$userid)
            abort(404);

        // get order details
        $orderinfo = Order::find($request->order_id);

        // get order product list ..
        $productList = OrderProducts::where('order_id', $request->order_id)->get();
        return $this->apiResponse($request, trans('language.message'), $productList, true);


    }

    // Questions
    public function Questions(Request $request)
    {
        $Questions = Question::orderBy('id', 'asc')->get();
        return $this->apiResponse($request, trans('language.message'), $Questions, true);
    }

    // whatsAppNumber
    public function whatsAppNumber(Request $request)
    {
        $whatsAppNumber = About::first()->whatsAppNumber;
        $data['whatsAppNumber'] = $whatsAppNumber;
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    // activate
    function activate(Request $request)
    {

        $request->validate([
            'code' => 'required|string|max:255',
        ]);

        $user = User::where('api_token', $request->api_token)->first();

        if (!$user)
            abort(404);

        if ($request->code == $user->code) {
            $user->status = "active";
            $user->mobile_verified_at = Carbon\Carbon::now();
            $user->save();
            return $this->apiResponse($request, trans('language.message'), null, true);

        } else {
            return $this->apiResponse($request, trans('language.message_error'), null, true);

        }


    }

    // resensActivateCode
    function resensActivateCode(Request $request)
    {
        $user = User::where('api_token', $request->api_token)->first();

        if (!$user)
            abort(404);

        // resend active Code ...
        //$data['code'] = rand(1111, 9999);
        $data['code'] = rand(1111, 9999);
        $user->code = "1234";
        $user->save();
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    // get Locations of company
    public function Locations(Request $request)
    {

        $about = About::first();
        $data['lng'] = $about->lng;
        $data['lat'] = $about->lat;
        return $this->apiResponse($request, trans('language.message'), $data, true);

    }

    // get bankAccountInformation
    public function bankAccountInformation(Request $request)
    {
        $about = About::first();
        $data['bankAccountNumber'] = $about->bankAccountNumber;
        $data['ibanNumber'] = $about->ibanNumber;
        $data['bankImage'] = $about->bankImage;
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    // user_notifications
    public function user_notifications(Request $request)
    {
        $userid = $request->user()->id;
        if (!$userid)
            abort(404);
        $data = User_notification::where('user_id', $userid)->orderBy('id', 'desc')->get();
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    // deleteUserNotifications
    public function deleteUserNotifications(Request $request)
    {
        $userid = $request->user()->id;
        if (!$userid)
            abort(404);

        $notifications = User_notification::where('user_id', $userid)->orderBy('id', 'desc')->get();
        foreach ($notifications as $notification) {

            User_notification::destroy($notification->id);
        }
        return $this->apiResponse($request, trans('language.message'), null, true);
    }


    // make order request
    public function makeOrder(Request $request)
    {

        $userid = $request->user()->id;
        if (!$userid)
            abort(404);

        $input['user_id'] = $userid;


        // get list of products
        $Orderproducts = json_decode($request->orderProducts)->orderProducts;
        // order data
        $order = [];
        $order['user_id'] = $userid;
        $order['order_status_id'] = "1"; // order in watting status ..
        $order['casheorder_id'] = "1"; // Cash Money
        $orderinfo = Order::create($order);


        $ordertotalPriceSum = 0; // to calc Total price of Order  ( totalPriceOfOrder )
        $ordertotalEarnestSum = 0;

        foreach ($Orderproducts as $Orderproduct) {


            // get product information
            $product = Product::find($Orderproduct->product_id);

            // check if this product has subcats ?


            if (isset($Orderproduct->productTypeId)) {

                // store product_id and order_id and count and earnest  in OrderProducts Table ..
                $OrderProductsdetails['product_id'] = $Orderproduct->product_id;
                $OrderProductsdetails['order_id'] = $orderinfo->id; // order id
                $OrderProductsdetails['count'] = $Orderproduct->count; // count of order
                $OrderProductsdetails['productType_id'] = $Orderproduct->productTypeId;  // save sub_cat of this product .
                // check if this product has earnest or not ( want to calculate earnest of this product )
                $productSubCatprice = SubCategory::find($Orderproduct->productTypeId)->price;
                $productPriceNew = $productSubCatprice;


            } else {

                // store product_id and order_id and count and earnest  in OrderProducts Table ..
                $OrderProductsdetails['product_id'] = $Orderproduct->product_id;
                $OrderProductsdetails['order_id'] = $orderinfo->id; // order id
                $OrderProductsdetails['count'] = $Orderproduct->count; // count of order
                // $OrderProductsdetails['productType_id'] = $Orderproduct->productTypeId;  // save sub_cat of this product .
                // check if this product has earnest or not ( want to calculate earnest of this product )

                if ($product->price == null) {

                    return $this->apiResponse($request, trans('language.BoPriceForthisProduct'), null, false);

                } else {
                    $productPriceNew = $product->price;
                }


            }


            // get product Price


            $OrderProductsdetails['productPrice'] = $productPriceNew;  // save current price of this order ...


            if ($Orderproduct->earnest == "1") {


                $OrderProductsdetails['earnest'] = $product->section->earnest;
                // calculate the minding of this product
                $mainPrice = $productPriceNew;
                $mainEarnest = $product->section->earnest;
                // calculate earnest of this product in numbers ..
                $productPriceAll = $productPriceNew * $Orderproduct->count;
                $earnestValue = $productPriceAll * ($product->section->earnest / 100);
                $currentPrice = $productPriceAll - $earnestValue;
                $OrderProductsdetails['residual'] = $currentPrice;

            } else {


                $OrderProductsdetails['earnest'] = "0";
                $OrderProductsdetails['residual'] = $productPriceNew;
                $earnestValue = 0;
                $currentPrice = $productPriceNew - $earnestValue;
            }


            OrderProducts::create($OrderProductsdetails);

            $ordertotalPriceSum = $ordertotalPriceSum + ($productPriceNew * $Orderproduct->count);

            // ordertotalEarnestSum
            $ordertotalEarnestSum = $ordertotalEarnestSum + $earnestValue;

        }

        $orderinfo->totalPriceOfOrder = $ordertotalPriceSum;
        $orderinfo->totalEarnestOfOrder = $ordertotalEarnestSum;
        $orderinfo->orderResidual = $ordertotalPriceSum - $ordertotalEarnestSum;
        $orderinfo->save();
        return $this->apiResponse($request, trans('language.message'), null, true);

    }

    // makeOrderViaBankTransfer


    // bankSaleRequest
    public function bankSaleRequest(Request $request)
    {
        $request->validate([
            'fromBank' => 'required',
            'accountName' => 'required',
            'accountNumber' => 'required',
            'transferAmount' => 'required',
            'requestImage' => 'required',
            'confirmMessage' => 'required',
        ]);

        $input = $request->all();

        if ($request->requestImage) {
            $input['requestImage'] = $this->storeFile($request->requestImage);
        }
        saleBankRequest::create($input);
        return $this->apiResponse($request, trans('language.message'), null, true);

    }


    public function makeOrderViaBankTransfer(Request $request)
    {

        $request->validate([
            'fromBank' => 'required',
            'accountName' => 'required',
            'accountNumber' => 'required',
            'transferAmount' => 'required',
            'requestImage' => 'required',
            'confirmMessage' => 'required',
        ]);

        $userid = $request->user()->id;
        if (!$userid)
            abort(404);

        $input['user_id'] = $userid;
        $input = $request->all(); // get data of request ..


        // get list of products
        $Orderproducts = json_decode($request->orderProducts)->orderProducts;
        // order data
        $order = [];
        $order['user_id'] = $userid;
        $order['order_status_id'] = "1";
        $order['casheorder_id'] = "2"; // Transform with bank
        $orderinfo = Order::create($order);


        $ordertotalPriceSum = 0; // to calc Total price of Order  ( totalPriceOfOrder )
        $ordertotalEarnestSum = 0;
        $finalEarnest = 0;

        foreach ($Orderproducts as $Orderproduct) {


            // get product information
            $product = Product::find($Orderproduct->product_id);

            // check if this product has subcats ?


            if (isset($Orderproduct->productTypeId)) {

                // store product_id and order_id and count and earnest  in OrderProducts Table ..
                $OrderProductsdetails['product_id'] = $Orderproduct->product_id;
                $OrderProductsdetails['order_id'] = $orderinfo->id; // order id
                $OrderProductsdetails['count'] = $Orderproduct->count; // count of order
                $OrderProductsdetails['productType_id'] = $Orderproduct->productTypeId;  // save sub_cat of this product .
                // check if this product has earnest or not ( want to calculate earnest of this product )
                $productSubCatprice = ProductSubCategory::where('product_id', $Orderproduct->product_id)
                    ->where('subCat_id', $Orderproduct->productTypeId)
                    ->first()->price;


                $productPriceNew = $productSubCatprice;


            } else {

                // store product_id and order_id and count and earnest  in OrderProducts Table ..
                $OrderProductsdetails['product_id'] = $Orderproduct->product_id;
                $OrderProductsdetails['order_id'] = $orderinfo->id; // order id
                $OrderProductsdetails['count'] = $Orderproduct->count; // count of order
                // $OrderProductsdetails['productType_id'] = $Orderproduct->productTypeId;  // save sub_cat of this product .
                // check if this product has earnest or not ( want to calculate earnest of this product )

                if ($product->price == null) {

                    return $this->apiResponse($request, trans('language.BoPriceForthisProduct'), null, false);

                } else {
                    $productPriceNew = $product->price;
                }


            }


            // get product Price


            $OrderProductsdetails['productPrice'] = $productPriceNew;  // save current price of this order ...


            if ($Orderproduct->earnest == "1") {


                //  $OrderProductsdetails['earnest'] = $product->section->earnest;
                // calculate the minding of this product
                //  $mainPrice = $productPriceNew;
                //  $mainEarnest = $product->section->earnest;
                // calculate earnest of this product in numbers ..
                $productPriceAll = $productPriceNew * $Orderproduct->count;
                $earnestValue = $productPriceAll * ($product->section->earnest / 100);
                $currentPrice = $productPriceAll - $earnestValue;
                $OrderProductsdetails['residual'] = $currentPrice;

                // return $this->apiResponse($request, trans('language.yes'), null, true);

            } else {

                //  return $this->apiResponse($request, trans('language.no'), null, true);

                $earnestValue = $productPriceNew * $Orderproduct->count;
                $OrderProductsdetails['residual'] = "0";

            }


            $OrderProductsdetails['earnest'] = $earnestValue;
            $finalEarnest += $earnestValue;


            OrderProducts::create($OrderProductsdetails);

            $ordertotalPriceSum = $ordertotalPriceSum + ($productPriceNew * $Orderproduct->count);

            // ordertotalEarnestSum
            $ordertotalEarnestSum = $ordertotalEarnestSum + $earnestValue;

        }

        $orderinfo->totalPriceOfOrder = $ordertotalPriceSum;
        $orderinfo->totalEarnestOfOrder = $finalEarnest;
        $orderinfo->orderResidual = $ordertotalPriceSum - $finalEarnest;
        $orderinfo->save();

        // save bank Transfer data
        if ($request->requestImage and is_file($request->requestImage)) {
            $input['requestImage'] = $this->storeFile($request->requestImage);
        }
        $input['order_id'] = $orderinfo->id;
        saleBankRequest::create($input);

        $data['order_id'] = $orderinfo->id;
        return $this->apiResponse($request, trans('language.message'), $data, true);

    }

    public function sms()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.hisms.ws/api.php?send_sms&username=966533381204&password=Mu1234567!0&numbers=+966504786502&sender=Mupsat&message=message&date=2015-1-30&time=24:01');
        // echo $res->getStatusCode();
        echo $res->getBody();
    }


}

