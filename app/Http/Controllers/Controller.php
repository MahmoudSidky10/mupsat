<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Factory;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;
use App\Notification;
use App\User_notification;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sms()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.hisms.ws/api.php?send_sms&username=966533381204&password=Mu1234567!0&numbers=+966504786502&sender=Mupsat&message=message&date=2015-1-30&time=24:01');
        // echo $res->getStatusCode();
        echo $res->getBody();
    }

    public function apiResponse($request, $message, $data, $successStatus)
    {
        $response['message'] = $message;
        if ($data != null)
            $response['data'] = $data;
        $response['success'] = $successStatus;
        return $response;
    }

    // send notification to this user ..
    public function sendNotification($message , $id)
    {
        $noti = new Notification ;
        $noti->body = $message ;
        $noti->title = 'Mupsat App';
        $noti->save();
        $usernoti = new User_notification;
        $usernoti->user_id = $id;
        $usernoti->notification_id = $noti->id;
        $usernoti->save();
    }

    public function storeFile($file)
    {
        $path = $file->store('public/files');
        $url = url('/');
        $url = str_replace('public', '', $url);
        $serverPath = $url . '/storage/app/';
        $path = $serverPath . $path;
        return $path;
    }

     public function store_image($image)
    {
        $imgPath = $image->store('public/images');
        $url = url('/');
        $url = str_replace('public', '', $url);
        $serverPath = $url . '/storage/app/';
        $path = $serverPath . $imgPath;
        return $path;
    }

   
    public function setUpFirebase()
    {
        $json = $this->fireBaseConnectionJson();
        $serviceAccount = ServiceAccount::fromJson($json);
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://mobset00.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();
        return $database;
    }

    function push_notification($tokens, $title, $message, $data = [])
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    function createCsvFile($array)
    {
        $fileName = "assets/receipts/order_" . $array["id"] . ".csv";
        $file = fopen($fileName, 'w');

        foreach ($array as $key => $value) {
            $data['key'] = $key;
            $data['value'] = $value;
            fputcsv($file, $data);
        }

        fclose($file);
    }

    public function sendSms($mobile , $message)
    {
        $user_name = "966533381204";
        $password = "123123@";
        $sender = "mupsat";
        $date = date("Y-m-d",time());
        $time = date("H:i",time());;
        $url = "https://www.hisms.ws/api.php?send_sms&username=$user_name&password=$password&numbers=$mobile&sender=$sender&message=$message&date=$date&time=$time";
        $client = new  Client();
        $res = $client->get($url);
        return $res->getBody()->getContents();
    }

}
