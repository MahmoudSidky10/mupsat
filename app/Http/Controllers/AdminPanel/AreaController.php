<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\Country;
use App\Area;
use Illuminate\Support\Facades\App;

class AreaController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $cities = City::orderBy('id','desc')->get();
        $countries = Country::orderBy('id','desc')->get();
        $areas = Area::orderBy('id','desc')->get();
        return view('AdminPanel.areas.index',compact('cities','countries','areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
         
        $input=$request->all();
        $input['country_id'] = City::find($request->city_id)->country_id;
        Area::create($input);
         session()->flash('success', trans('language.done'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

 
        $data = $request->all();
        $data['country_id'] = City::find($request->city_id)->country_id;
        $item = Area::find($id);
        $item->update($data);
        session()->flash('success',trans('language.done'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
 
        $delete = Area::destroy($id);
            session()->flash('success',trans('language.done'));
        
        return back();
    }


}