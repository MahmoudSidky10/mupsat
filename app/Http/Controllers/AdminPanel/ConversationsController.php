<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Conversation;
use App\Message;

class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conversations = Conversation::orderBy('id','desc')->get();
        return view('AdminPanel.conversations.index',compact('conversations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function openConversation($id)
    {
        // get all messages in this converstaion .

        $messages = Message::where('conversation_id', $id)->get();

        return view('AdminPanel.conversations.chat',compact('messages'));

    }

    public function saveMessage(Request $request)
    {
        

        // Send message to user (  message  )
        $sender_id =  $request->admin;
        $receiver_id  =  $request->userid; 
        $body = $request->messgae;
        $checkConversavtion = Conversation::where('sender_id', $sender_id)
        ->where('receiver_id', $receiver_id)->first();

        if($checkConversavtion)
        {

        // Create new $message in Exist coversation ..
        $message = new Message();
        $message->conversation_id = $checkConversavtion->id;
        $message->message = $body;
        $message->sendBy = 'admin';
        $message->save();

        }else{

        // Create new $conversation ..
        $conversation  = new Conversation() ;
        $conversation->sender_id = $sender_id ;
        $conversation->receiver_id = $user->id;
        $conversation->save();
        // Create new $message
        $message = new Message();
        $message->conversation_id = $conversation->id;
        $message->message = $body;
        $message->sendBy = 'admin';
        $message->save();

        }
        
 
        session()->flash('success', trans('تم ارسال التنبيه بنجاح '));
 

        return back();

    }
}
