<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Section;
use App\SubCategory;
use App\ProductSubCategory;
use LaravelLocalization;
use Illuminate\Support\Facades\App;

class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(20);
        $subcategories = SubCategory::orderBy('id', 'desc')->get();
        $categories = Section::all();
        return view('AdminPanel.products.index', compact('products', 'categories', 'subcategories'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {


        if($request->section_id == 0)
        {
            session()->flash('success', trans('لا يمكن اضافه منتج دون تحديد القسم الرئيسي له .'));
            return back();
        }


        $input = $request->all();

        if (!$request->minimumCount)
            $input["minimumCount"] = 1;

        if (!$request->maximumCount)
            $input["maximumCount"] = 1;

        if (!$request->increasingBy)
            $input["increasingBy"] = 1;

        if (!$request->rate)
            $input["rate"] = 1;


        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }
        if ($request->cover_image) {
            $input['cover_image'] = $this->store_image($request->cover_image);
        }

        if (!$request->price) {
            $input['price'] = null;
        }

        $product = Product::create($input);


        if ($request->productprice != null) {

            // create product subcats with own price ..
            $subCats = $request->subCats;
            $productprice = $request->productprice;


            foreach ($productprice as $price) {

                if ($price != null) {
                    $validprices[] = $price;
                }
            }


            //create new ProductSubCategory
            $count = 0;
            if (is_array($subCats))
                $count = count($subCats);

            //dd($subCats , $validprices);

            for ($i = 0; $i < $count; $i++) {
                // store data ..
                $item['product_id'] = $product->id;
                $item['subCat_id'] = $subCats[$i];
                $item['price'] = $validprices[$i];
                ProductSubCategory::create($item);
            }
        }


        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);


        session()->flash('success', trans('language.product_add'));
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        if($request->section_id == 0)
        {
            session()->flash('success', trans('لا يمكن اضافه منتج دون تحديد القسم الرئيسي له .'));
            return back();
        }
        $input = $request->all();


        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

        if ($request->cover_image) {
            $input['cover_image'] = $this->store_image($request->cover_image);
        }


        $item = Product::find($id);
        $item->update($input);

        // update productsubcatPrice ?
        if ($request->productprice != null) {

            // create product subcats with own price ..
            $subCats = $request->productids;
            $ids = $request->productids;
            $productprice = $request->productprice;


            foreach ($productprice as $price) {

                if ($price != null) {
                    $validprices[] = $price;
                }
            }


            //create new ProductSubCategory
            $count = 0;
            if (is_array($subCats))
                $count = count($subCats);

            //dd($subCats , $validprices);

            for ($i = 0; $i < $count; $i++) {
                // store data ..
                $subobj = ProductSubCategory::find($ids[$i]);
                $subobj->price = $validprices[$i];
                $subobj->save();
            }
        }


        session()->flash('success', trans('language.product_edit'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        Product::destroy($id);

        session()->flash('success', trans('language.product_delete'));
        return back();
    }
}
