<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $users = User::orderBy('id', 'desc')->where('type','family')->get();
        return view('AdminPanel.users.family',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

        $input['api_token'] = rand(99999999, 999999999) . time();
        $input['type'] = "family";
        User::create($input);
 
         session()->flash('success', trans('تتم اضافه  مستخدم جديده'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

         $item = User::find($id);
        $item->update($input);
        session()->flash('success', trans('تم تعديل بيانات المستخدم .'));
        return redirect('technicians');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = User::destroy($id);
         
         session()->flash('success', trans('تم حذف المستخدم'));

           // return to a specific view
           return back();
    }
}
