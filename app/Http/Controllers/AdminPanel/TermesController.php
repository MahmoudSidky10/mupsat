<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Terme;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use Illuminate\Support\Facades\App;

class TermesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
    {
        $termes = Terme::find(1);
        return view('AdminPanel.termes.index',compact('termes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $termes = Terme::find($id);
        $termes->termes_ar = $request->termes_ar;
        $termes->termes_en = $request->termes_en;
        $termes->save();
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Terme::destroy($id);
session()->flash('success', trans('language.done'));
        return back();
    }
}