<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use Illuminate\Support\Facades\App;
use App\SocialMedia;

class SoicalMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $soicalMedias = SocialMedia::orderBy('id','desc')->get();
        return view('AdminPanel.soicalMedias.index',compact('soicalMedias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }


        SocialMedia::create($input);

        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
    
        session()->flash('success', trans('language.done'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

         $item = SocialMedia::find($id);
        $item->update($input);
        
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
 
        session()->flash('success', trans('language.done'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SocialMedia::destroy($id);
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
    
        session()->flash('success', trans('language.done'));
        return back();
    }
}
