<?php

namespace App\Http\Controllers\AdminPanel;

use App\saleBankRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderProducts;
use App\Product;
use LaravelLocalization;
use Illuminate\Support\Facades\App;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all orders in db: -
        $orders = Order::orderBy('id','desc')->get();
        return view('AdminPanel.orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Order::destroy($id);
        session()->flash('success',trans('language.done'));
        return back();
    }

    public function deleteorder($id)
    {
         $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        Order::destroy($id);
        session()->flash('success',trans('language.done'));
        return back();
    }

    //deactive_order 
    public function deactive_order($id)
    {
        $order = Order::find($id);
        $order->status = 'cancel';
        $order->save();
        session()->flash('success',trans('language.done'));
        return back();
    }

    //getProducts
    public function getProducts(Request $request)
    {
        $products = Product::where('section_id',$request->id)->get();
        return response()->json(['products' => $products]);
    }
    //storeOrder
    public function storeOrder(Request $request)
    {
          $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        $input = $request->all();

        $order = Order::create($input);
        session()->flash('success',trans('language.done'));
        return back();
    }

    // get order Products 
    public function orderProducts($id)
    {
        $products = OrderProducts::where('order_id',$id)->get();
        $orderId = $id;
        return view('AdminPanel.orders.products',compact('products','orderId'));

    }

    // orderBankInformation
    public function orderBankInformation($id)
    {
        $bankinfo = saleBankRequest::where('order_id',$id)->first();
        return view('AdminPanel.orders.bankinfo',compact('bankinfo'));

    }
}
