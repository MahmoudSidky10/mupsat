<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;
use Illuminate\Support\Facades\App;
use LaravelLocalization;

class AboutMapsutController extends Controller
{
    //

     public function index(){


        $about = About::find(1);

        return view('AdminPanel.about.index',compact('about'));

    }



    public function update(Request $request , $id){
  
 
        $about = About::find($id);
        $about->name_ar = $request->name_ar;
        $about->name_en = $request->name_en;
        $about->save();

         $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

         // flashing a success message
                session()->flash('success' , trans('language.done'));


                return back();

    }

}
