<?php

namespace App\Http\Controllers\AdminPanel;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use LaravelLocalization;
use Illuminate\Support\Facades\App;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        $users = User::orderBy('id', 'desc')->where('type', 'client')->paginate(10);
        return view('AdminPanel.users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);

        $input = $request->all();

        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

        $input['api_token'] = rand(99999999, 999999999) . time();
        $input['type'] = "client";
        $input['password'] = bcrypt($request->password);
        User::create($input);

        session()->flash('success', trans('language.user_add'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $input = $request->all();
        if ($request->image) {
            $input['image'] = $this->store_image($request->image);
        }

        if ($request->password) {
            $input['password'] = bcrypt($request->password);
        }

        $item = User::find($id);
        $item->update($input);

        session()->flash('success', trans('language.user_edit'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);


        $delete = User::destroy($id);

        session()->flash('success', trans('language.user_deleted'));


        // return to a specific view
        return back();

    }

}
