<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use App\User_notification;
use App\User;
use LaravelLocalization;
use Illuminate\Support\Facades\App;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 

        $users = User::where('type','client')->orderBy('id', 'desc')->get();
        $technicians = User::where('type','family')->orderBy('id', 'desc')->get();
        $all = User::where('type','!=','admin')->orderBy('id', 'desc')->get();

        $notifications = Notification::orderBy('id', 'desc')->get();

        return view('AdminPanel.notifications.index', compact('notifications','users','technicians','all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $ids = $request->technician;


 $lang = LaravelLocalization::getCurrentLocale(); 

      App::setLocale($lang);
         

         foreach($ids as $id)
         {

                    
                    $user = User::find($id);
                    
                    $title = $request->title;
                    $body = $request->body;

                    $data['title'] = $title;
                    $data['body'] = $body;

                    $notification = new Notification();
                    $notification->title = $title;
                    $notification->body = $body;
                    $notification->save();

                    $User_notification = new User_notification();
                    $User_notification->user_id = $user->id;
                    $User_notification->notification_id = $notification->id;
                    $User_notification->save();

                    // send push notification with Fcm
                    $this->push_notification([$user->fire_base_token], $title, $body, $data);            

       }
 

       

              session()->flash('success',trans('language.done'));
               return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
