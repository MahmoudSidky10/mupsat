<?php

namespace App\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Carbon;
use App\Mail\VerifyMail;
use App\User;
use App\Order;
use App\Product;
use App\Categorie;
use App\Conversation;
use App\Message;
use App\Reporting;
use App\ContactWithApp;
use App\SubCategory;
use App\Notification;
use App\User_notification;
use App\userFavouriteProducts;
use LaravelLocalization;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{



    public function homepage(){

        return view('site.home');
    }

    // sitecontact
    public function sitecontact(Request $request)
    {
        // save contct us messages ..
        $input = $request->all();
        ContactWithApp::create($input);
        return back();
    }
    // verifyEmail
    public function verifyEmail($id)
    {
        // get user with id :-
        $user = User::find($id);
        $user->email_verified_at = Carbon\Carbon::now();
        $user->save();
        return view('AdminPanel.email_verified', compact('user'));

    }

    // userProduct
    public function userProduct($id)
    {   
        $categories = Categorie::all();
        $products = Product::where('user_id',$id)->get();
        return view('AdminPanel.products.familyProducts',compact('products','categories'));
    }

    public function resetPass(Request $request, $id)
    {
        // get user with id :-
        $user = User::find($id);
        return view('AdminPanel.resetPasswordForm', compact('user'));
    }

    public function politics()
    {
        return view('site.politics');
    }

    public function home()
    {
        return view('site.home');
    }

    public function login(Request $request)
    {


        $email = $request['email'];
        $password = $request['password'];


        if (empty($email)) {
            return response()->json(['status' => 'empty_txt_user_check']);
        }


        if (empty($password)) {
            return response()->json(['status' => 'empty_password']);
        }


        // we should acess admin table and compare email , password
        // if it true ->  will go to dashboard page .

        //  Auth::guard('admin')->attempt


        if (Auth::attempt(['email' => $email, 'password' => $password, 'type' => 'admin'])) {


            // here attemp is successfull then check user type  :-
            // auth attemp success as email

            $user = Auth::user();

            Auth::login($user);


            // now you are Admin so you can enter the Dashboard ...

            return redirect()->intended('/dash');

        } else {


            // if this person not admin ?
            // we will send him to ( error page [ location] =>   AdminPanel ->  errors -> notallowed.blade.php )

            return view('AdminPanel.errors.notallowed');
        }

    }

    public function index(Request $request)
    {

        // go to dashboard page :-

        return view('AdminPanel.Dashboard.index');

    }

    public function userOrder(Request $request, $id)
    {
        // Get all order of this user :-
        $orders = Order::where('user_id', $id)->get();

        return view('AdminPanel.users.orders', compact('orders'));
    }

    public function techniciansOrder(Request $request, $id)
    {

        // Get all order of this user :-
        $orders = Order::where('user', $id)->get();

        return view('AdminPanel.users.orders', compact('orders'));
    }

    // userFavourite
    public function userFavourite(Request $request , $id)
    {
        $favourites = userFavouriteProducts::where('user_id', $id)->get();

        return view('AdminPanel.users.favourite', compact('favourites'));
    }
    //deletefavourite
    public function deletefavourite($id)
    {

        userFavouriteProducts::destroy($id);
        session()->flash('success', trans('تم مسح المنتج من المضله بنجاح .'));
        return back();
    }

    public function contacts()
    {

        $contacts = ContactWithApp::orderBy('id', 'desc')->get();
        return view('AdminPanel.about.contactus', compact('contacts'));
    }

    public function deletecontacts()
    {
        $contacts = ContactWithApp::all();

        foreach ( $contacts as $contact) {

            ContactWithApp::destroy($contact->id);
        }

        session()->flash('success', trans('language.done'));
        return back();
    }

     public function deletecontactsid($id)
    {
        
        ContactWithApp::destroy($id);

        session()->flash('success', trans('language.done'));
        return back();
    }

    // deleteReports
    public function  deleteReports(Request $request)
    {

        $Reportings = Reporting::all();

        foreach ( $Reportings as $Reporting) {

            Reporting::destroy($Reporting->id);
        }

        session()->flash('success', trans('تم حذف جميع  التبيلغات .'));
        return back();

    }

    public function notificationMessage(Request $request, $id)
    {

        //send notification

         

        $user = User::find($id);      

        $title = trans('language.dashboard') ;

        $body = $request->messgae;

        $data['title'] = $title;
        $data['body'] = $body;

        $notification = new Notification();
        $notification->title = $title;
        $notification->body = $body;
        $notification->save();

        $User_notification = new User_notification();
        $User_notification->user_id = $user->id;
        $User_notification->notification_id = $notification->id;
        $User_notification->save();
 
        session()->flash('success', trans('language.done'));
 

        return back();

    }

    // UsersnotificationMessage for all users 

    public function UsersnotificationMessage(Request $request)
    {
         
         $users  = User::all();

         foreach ($users as $userinfo) {
             
        $user = User::find($userinfo->id);      

        $title = trans('language.dashboard') ;

        $body = $request->messgae;

        $data['title'] = $title;
        $data['body'] = $body;

        $notification = new Notification();
        $notification->title = $title;
        $notification->body = $body;
        $notification->save();

        $User_notification = new User_notification();
        $User_notification->user_id = $user->id;
        $User_notification->notification_id = $notification->id;
        $User_notification->save();

         }

 
        session()->flash('success', trans('language.done'));
 

        return back();

    }

    public function userMessage(Request $request, $id)
    {

        //send notification

        $user = User::find($id);      

        $title =  trans('language.dashboard');

        $body = $request->messgae;

        $data['title'] = $title;
        $data['body'] = $body;

        $notification = new Notification();
        $notification->title = $title;
        $notification->body = $body;
        $notification->save();

        $User_notification = new User_notification();
        $User_notification->user_id = $user->id;
        $User_notification->notification_id = $notification->id;
        $User_notification->save();

        // Send message to user (  message  )
        $sender_id = Auth::user()->id;
        $receiver_id  =  $user->id; 
        $checkConversavtion = Conversation::where('sender_id', $sender_id)
        ->where('receiver_id', $receiver_id)->first();

        if($checkConversavtion)
        {

        // Create new $message in Exist coversation ..
        $message = new Message();
        $message->conversation_id = $checkConversavtion->id;
        $message->message = $body;
        $message->sendBy = 'admin';
        $message->save();

        }else{

        // Create new $conversation ..
        $conversation  = new Conversation() ;
        $conversation->sender_id = $sender_id ;
        $conversation->receiver_id = $user->id;
        $conversation->save();
        // Create new $message
        $message = new Message();
        $message->conversation_id = $conversation->id;
        $message->message = $body;
        $message->sendBy = 'admin';
        $message->save();

        }
        
 
        session()->flash('success', trans('language.done'));
 

        return back();

    }


    public function active_Tech($id)
    {

        $user = User::find($id);
        $user->status = "active";
        $user->save();

        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
 
        session()->flash('success', trans('language.active_Tech'));
        return back();
    }

 
    public function Not_active_Tech($id)
    {

        $user = User::find($id);
        $user->status = "not_active";
        $user->save();
        
        $lang = LaravelLocalization::getCurrentLocale();
        App::setLocale($lang);
 
        session()->flash('success', trans('language.Not_active_Tech'));
        return back();

    }

    //getreports
    public function getreports()
    {
        $reports = Reporting::orderBy('id','desc')->get();
        return view('AdminPanel.reporting.index',compact('reports'));
    }
    // Deletereport
    public function Deletereport(Request $request , $id)
    {
        Reporting::destroy($id);
         session()->flash('success', trans('تتم حذف البلاغ بنجاح . '));
        return back();
    }

    // getSubCategories
    public function getSubCategories(Request $request){

        $mainCatId = $request->category_id ;
        $categories = SubCategory::where('section_id',$mainCatId)->get();
        return response()->json(['categories' => $categories]);

    }

}
