<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }
}
