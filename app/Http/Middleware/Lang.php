<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    public function handle($request, Closure $next)
    {
        $languages = ['ar', 'en'];
        $lang = $request->lang;
        if (!in_array($lang, $languages))
            $lang = "ar";
        app()->setLocale($lang);
        return $next($request);
    }
}
