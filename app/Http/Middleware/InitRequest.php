<?php

namespace App\Http\Middleware;

use App\Default_image;
use App\Http\Controllers\Controller;
use Closure;

class InitRequest extends Controller
{
    public function handle($request, Closure $next)
    {
        $data = $request->all();

        if ($request->image)
            $data['image'] = $this->storeFile($request->image);

        if ($request->password)
            $data['password'] = bcrypt($request->password);
        if ($request->name)
            $data['first_name'] = $request->name;

        if ($request->from)
            $data['from'] = str_limit($request->from, 10);

        if ($request->to)
            $data['to'] = str_limit($request->to, 10);

        if ($request->visit_time)
            $data['visit_time'] = str_limit($request->visit_time, 10);


        $request['data'] = $data;
        return $next($request);
    }
}
