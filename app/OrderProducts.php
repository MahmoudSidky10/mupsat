<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    protected $fillable = [
        'product_id',
        'order_id',
        'count',
        'earnest',
        'productPrice',
        'productType_id',
        'residual',
    ];
    public function toArray()
    {
         $data['id']    = $this->id;
         $data['time']  = strtotime($this->created_at) * 1000 ;
        // product info
        $data['product_id'] = $this->product->id;
        $data['productName'] = $this->product->name;
        $data['productCode'] = $this->product->code;
        $data['productCount'] = $this->count;
        $data['ProductTotalPrice'] = $this->count * $this->product->price;
        $data['totalEarnest'] = $this->order->totalEarnestOfOrder ;
        $data['orderResidual'] = $this->order->orderResidual ;
        $data['product_price'] = $this->product->price;
        $data['product_image'] = $this->product->image;
        return $data;
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }


    public function getTotalCostAttribute()
    {
        $attribute = "غير محدد ";
        $product = Product::find($this->product_id);
        if (!$product)
            return $attribute;
        $attribute = (float)$product->price * $this->count;
        return $attribute;
    }
}
