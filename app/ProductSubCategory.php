<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    public $timestamps = false;

	protected $fillable = [
	        'product_id',
	        'subCat_id',
	        'price'
	];

	public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }


    public function toArray()
    {
        $data['id'] = $this->subcat->id;
        $data['name'] = $this->subcat->name;
        $data['price'] = $this->price;
        return $data;
    }

    public function subcat()
    {
        return $this->belongsTo('App\SubCategory', 'subCat_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
