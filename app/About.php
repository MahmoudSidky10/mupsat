<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    

    protected $table = "abouts";
    public $timestamps = false;
    protected $fillable = [
        'name_ar',
        'name_en',
        'whatsAppNumber',
        'lng',
        'lat',
        'bankAccountNumber',
        'ibanNumber',
        'bankImage',
    ];
    public function toArray()
    {
        $data['name'] = $this->name;
        return $data;
    }
    // custom Attributes
    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

}
