<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactWithApp extends Model
{

//    public function toArray()
//    {
//        $data['id'] = $this->id;
//        $data['name'] = $this->id;
//        $data['email'] = $this->id;
//        $data['message'] = $this->id;
//    }
    protected $fillable = [
        'name',
        'email',
        'message',
        'title',
        'number',
        'surname',
    ];
    
     
}
