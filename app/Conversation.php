<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    //

    protected $fillable = [
       
        'sender_id',
        'receiver_id',
    ];

    public function toArray()
    {

        $data['sender'] = $this->sender;
        $data['receiver'] = $this->receiver;
        return $data;
    }


      public function sender()
    {
        return $this->belongsTo('App\User','sender_id');
    }

      public function receiver()
    {
        return $this->belongsTo('App\User','receiver_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Conversation', 'conversation_id');
    }


}
