<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name_ar',
        'name_en',
        'unit_ar',
        'unit_en',
        'size_unit_en',
        'size_unit_ar',
        'image',
        'priceUnit_ar',
        'priceUnit_en',
        'earnest',
        'typeSizeTitle_ar',
        'typeSizeTitle_en'
    ];
    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getTypeSizeTitleAttribute()
    {
        if (\request()->lang == "en")
            return $this->typeSizeTitle_en;
        else
            return $this->typeSizeTitle_ar;
    }

    

     public function getUnitAttribute()
    {
        if (\request()->lang == "en")
            return $this->unit_en;
        else
            return $this->unit_ar;
    }
    public function getSizeAttribute()
    {
        if (\request()->lang == "en")
            return $this->size_unit_en;
        else
            return $this->size_unit_ar;
    }

    

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['unit'] = $this->unit;
        $data['size'] = $this->size;
        $data['image'] = $this->image;
        $data['isEarnest'] = $this->Isearnest();
        $data['earnest'] = $this->earnest;
        $data['priceUnit'] = $this->getPriceUnitAttribute();
        $data['typeSizeTitle'] = $this->getTypeSizeTitleAttribute();
         
        return $data;
    }


    public function getPriceUnitAttribute()
    {
        if (\request()->lang == "en")
            return $this->priceUnit_en;
        else
            return $this->priceUnit_ar;
    }


    public  function Isearnest()
    {
        if($this->earnest != "0" )
        {
            return true;
        }else{
            return false;
        }
    }
}
