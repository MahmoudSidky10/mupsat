<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saleBankRequest extends Model
{


    protected $fillable = [

        'fromBank',
        'accountName',
        'accountNumber',
        'transferAmount',
        'requestImage',
        'confirmMessage',
        'order_id',
    ];


}
