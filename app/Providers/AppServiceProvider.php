<?php

namespace App\Providers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected $lang;

    public function boot(Request $request)
    {
        $this->lang =  $this->lang($request);
        app()->singleton('lang', function () {
            return $this->lang;
        });

        Schema::defaultStringLength(191);
    }

    public function lang(Request $request)
    {
        if ($request->api_token) {
            $lang = "ar";
            if ($request->lang)
                if ($request->lang == "ar" or $request->lang == "en")
                    $lang = $request->lang;
        } else {
            $lang = $request->lang;
            if ($lang != "ar" and $lang != "en")
                $lang = "ar";
        }
        return $lang;
    }

    public function register()
    {
        //
    }
}
