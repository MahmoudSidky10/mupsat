<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //


    protected $fillable = [
        'id',
        'user_id',
        'order_status_id',
        'totalPriceOfOrder',
        'casheorder_id',
    ];
    public function toArray() // client order api return
    {
        $data['id'] = $this->id;
        $data['status'] = $this->order_status->name;
        $data['totalPriceOfOrder'] = $this->totalPriceOfOrder;
        $data['totalEarnestOfOrder'] = $this->totalEarnestOfOrder;
        $data['orderResidual'] = $this->orderResidual;
        $data['time'] = strtotime($this->created_at) * 1000 ;

        return $data;
    }

    public function orderobj(){ // client order Details api return

        $data['id'] = $this->id;
                         

        return $data;
    }


  

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order_status()
    {
        return $this->belongsTo('App\Order_status', 'order_status_id');
    }
    public function OrderProducts()
    {
        return $this->hasMany('App\OrderProducts', 'order_id');
    }
    public function productDetails()
    {
        $items  = OrderProducts::where('order_id',$this->id)->get();
        return $items;
    }
    // getTotalCostAttribute
    public function getTotalCostAttribute()
    {
        $productsInOrderProducts = OrderProducts::where('order_id',$this->id)->get();
        $sum = 0 ;
        foreach($productsInOrderProducts as $item)
        {

            $sum = $sum +  (float)$item->product->price * $item->count;
        }

        return $sum;

    }

     
}
