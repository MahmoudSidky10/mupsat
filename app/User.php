<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    // User.php
    use Notifiable;

    protected $fillable = [

        'first_name',
        'last_name',
        'image',
        'city_id',
        'area_id',
        'type',
        'status',
//        'email',
        'mobile',
        'code',
        'passCode',
        'email_verified_at',
        'mobile_verified_at',
        'password',
        'device_type',
        'fire_base_token',
        'api_token',

    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['api_token'] = $this->api_token;
        $data['first_name'] = $this->first_name;
        $data['last_name'] = $this->last_name;
       // $data['image'] = $this->image;
        $data['cityName'] = $this->city;
        $data['countryName'] = $this->city->country->name;
        $data['type'] = $this->type;
        $data['status'] = $this->status;
//        $data['email'] = $this->email;
        $data['mobile'] = $this->mobile;
        $data['email_verified_at'] = $this->isEmailVerified();
        $data['mobile_verified_at'] = $this->isMobileVerified();

        return $data;
    }

    public function isEmailVerified()
    {
        if($this->email_verified_at != null){
            return true;
        }else
        {
            return false;
        }
    }

     public function isMobileVerified()
    {
        if($this->mobile_verified_at != null){
            return true;
        }else
        {
            return false;
        }
    }

    public function  profile()
    {
        $data['id'] = $this->id;
        $data['first_name'] = $this->first_name;
        $data['last_name'] = $this->last_name;
        //$data['image'] = $this->image;
//        $data['email'] = $this->email;
        $data['mobile'] = $this->mobile;
        $data['cityId'] = $this->city->id;
        $data['countryId'] = $this->city->country->id;
        $data['areaId'] = (Int)$this->area_id;
        $data['cityName'] = $this->city->name;
        $data['countryName'] = $this->city->country->name;
        $data['areaName'] = $this->area->name;

        return $data;

    }



    public function city()
    {
        return $this->belongsTo('App\City','city_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Area','area_id');
    }

}
