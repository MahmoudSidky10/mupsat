<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */

    public function render($request, Exception $exception)
    {
        if ($request->api_token or $request->fire_base_token) {
            if ($exception instanceOf ValidationException) {
                $errors = $exception->errors();
                foreach ($errors as $error) {
                    $data['message'] = $error[0];
                }
            } else
                $data['message'] = $exception->getMessage();
            $data['success'] = false;
            return response()->json($data, 200);
        }
        return parent::render($request, $exception);
    }

}
