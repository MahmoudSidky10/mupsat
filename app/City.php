<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    public $timestamps = false;
    protected $fillable = [
        'name_ar',
        'name_en',
        'country_id',
        'code',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['areas'] = $this->areas;
        return $data;
    }

    public function getNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getCurrencyAttribute()
    {
        $attribute = __('language.currencyKD');
        if ($this->country)
            $attribute = $this->country->currency;
        return $attribute;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function areas()
    {
        return $this->hasMany(Area::class);
    }
}
