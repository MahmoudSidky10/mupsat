<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //

     protected $fillable = [
       
        'conversation_id',
        'message',
        'sendBy',
    ];

    public function toArray()
    {
        $data['conversation_id'] = $this->conversation_id;
        $data['conversation'] = $this->Conversation;
        $data['message'] = $this->message;
        $data['sendBy'] = $this->sendBy;
        return $data;
    }

    public function Conversation()
    {
        return $this->belongsTo('App\Conversation', 'conversation_id');
    }
}
