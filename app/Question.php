<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    
     protected $fillable = [
        'title_ar',
        'title_en',
        'body_ar',
        'body_en',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['title'] = $this->title;
        $data['body'] = $this->body;
        $data['time'] = strtotime($this->created_at) * 1000;
        return $data;
    }

    public function getTitleAttribute()
    {
        if (\request()->lang == "en")
            return $this->title_en;
        else
            return $this->title_ar;
    }

    public function getBodyAttribute()
    {
        if (\request()->lang == "en")
            return $this->body_en;
        else
            return $this->body_ar;
    }

    
}
