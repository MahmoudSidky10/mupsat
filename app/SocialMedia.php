<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    //

    public $timestamps = false;

    protected $fillable = [
        'name_ar',
        'name_en',
        'link',
        'image',
        'color'
    ];
    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

     

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['link'] = $this->link;
        $data['image'] = $this->image;
        $data['color'] = $this->color;
        return $data;
    }
}
