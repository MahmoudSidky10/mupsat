<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //

	protected $table="sub_categories";

	public $timestamps = false;

	protected $fillable = [
	        'name_ar',
	        'name_en',
	        'price',
	        'section_id',
	];

	public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }


    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['price'] = $this->price;
        return $data;
    }

    public function Category()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }


}
