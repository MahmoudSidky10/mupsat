<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_status extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name_ar',
        'name_en',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        return $data;
    }

    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }
}
