<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleTerme extends Model
{
    protected $fillable = [

        'termes_en',
        'termes_ar',
    ];

    public function toArray()
    {
         
        $data['name'] = $this->getNameAttribute();
        return $data;
    }

    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->termes_en;
        else
            return $this->termes_ar;
    }
}
