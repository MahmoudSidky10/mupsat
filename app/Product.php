<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    //
    protected $fillable = [
        'name_en',
        'name_ar',
        'price',
        'image',
        'cover_image',
        'code',
        'rate',
        'special',
        'details_en',
        'details_ar',
        'size_ar',
        'size_en',
        'section_id',
        'minimumCount',
        'maximumCount',
        'increasingBy',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['image'] = $this->image;
       if($this->price == null)
        {
            $data['price'] = "السعر علي حسب النوع ";
        }else{
             $data['price'] = $this->price;
        }
        $data['code'] = "#" . $this->code;
        $data['currency'] = $this->currency();
        $data['priceUnit'] = $this->getPriceUnitAttribute();
       

        return $data;
    }

    // return one object from product .. 
    public function productObj()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['details'] = $this->details;
        $data['image'] = $this->image;
        $data['cover_image'] = $this->cover_image;
        $data['size'] = $this->size;
        $data['code'] = "#" . $this->code;
        $data['rate'] = $this->rate;
        $data['section_id'] = $this->section_id;
        $data['section_name'] = $this->section->name;
        $data['section_unit'] = $this->section->unit;
        $data['sectionIsEarnest'] = $this->section->Isearnest();
        $data['sectionEarnest'] = $this->section->earnest;
        $data['currency'] = $this->currency();
        $data['minimumCount'] = $this->minimumCount;
        $data['maximumCount'] = $this->maximumCount;
        $data['increasingBy'] = $this->increasingBy;
        $data['priceUnit'] = $this->getPriceUnitAttribute();
        $data['typeSizeTitle'] = $this->section->getTypeSizeTitleAttribute();
       

        if($this->price == null)
        {
            $data['price'] = trans('language.Price_by_type');;
        }else{
             $data['price'] = $this->price;
        }
    

        if(count($this->getsubCategories()) > 0)
        {
            // subCat details
        $data['productType']['name'] = $this->section->size;
        $data['productType']['types'] = $this->getsubCategories();
        }
        
        return $data;
    }



    public function getsubCategories(){

        $subCategories = ProductSubCategory::OrderBy('id','asc')->where('product_id',$this->id)->get();
        return $subCategories;
    }

 
    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getPriceUnitAttribute()
    {
        if (\request()->lang == "en")
            return $this->section->priceUnit_en;
        else
            return $this->section->priceUnit_ar;
    }

    public function getSizeAttribute()
    {
        if (\request()->lang == "en")
            return $this->size_en;
        else
            return $this->size_ar;
    }

    public function getDetailsAttribute()
    {
        if (\request()->lang == "en")
            return $this->details_en;
        else
            return $this->details_ar;
    }

    public function currency()
    {
        if (\request()->lang == "en")
            return "SR";
        else
            return "رس";
    }

    public function getFormateSectionNameAttribute()
    {
        $attribute = __('language.empty');
        if ($this->section)
            $attribute = $this->section->name;
        return $attribute;
    }



    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

    


}
