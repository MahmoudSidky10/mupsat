<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //

    protected $fillable = [
        'image',
        'lang',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['image'] = $this->image;
        return $data;
    }

    
}
