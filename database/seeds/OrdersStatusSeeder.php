<?php

use Illuminate\Database\Seeder;

class OrdersStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
    {
        // 0 new order

        $this->add('تم استلام الطلب  ', 'accepted'); //1
        $this->add('جارى تنفيذ الطلب  ', 'run'); //2
        $this->add('تم الإنتهاء من الطلب', 'done'); //3
        $this->add('الطلب فى الطريق للعميل ', 'delivering'); //4
        $this->add('نم الغاء الطلب . ', 'cancel'); //5
        $this->add('تم الغاء الطلب من قبل الاداره .', 'adminReject'); //6
    }

        public function add($name_ar, $name_en)
    {
        $data['name_ar'] = $name_ar;
        $data['name_en'] = $name_en;
        \App\Order_status::create($data);
    }

}
