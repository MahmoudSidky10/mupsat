<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    public function run()
    {
        $this->add('تطبيق ويدنق يرحب بكم اجمل ترحيب و تتمنى لكم زفاف سعيد');
    }

    public function add($name)
    {
        $data['name_ar'] = $name;
        $data['name_en'] = $name;
        $data['whatsAppNumber'] = "0511516652";
        \App\About::create($data);
    }
}
