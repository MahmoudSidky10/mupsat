<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    public function run()
    {
        $data['image'] = "https://images-na.ssl-images-amazon.com/images/I/31aiVoYfpuL._SX355_.jpg";
        $data['name_ar'] = "مصر";
        $data['name_en'] = "Egypt";
        $data['currency_ar'] ="جنيه";
        $data['currency_en'] = "Pound";
        $data['code_name'] ="LE";
        $data['code'] = "+02";
        \App\Country::create($data);
        $data['image'] = "http://iuvmpress.com/hi/wp-content/uploads/2018/08/%E0%A4%B8%E0%A4%8A%E0%A4%A6%E0%A5%80-%E0%A4%85%E0%A4%B0%E0%A4%AC-%E0%A4%95%E0%A5%80-%E0%A4%B0%E0%A4%BE%E0%A4%9C%E0%A4%A7%E0%A4%BE%E0%A4%A8%E0%A5%80-%E0%A4%B0%E0%A4%BF%E0%A4%AF%E0%A4%BE%E0%A4%9C%E0%A4%BC1.jpg";
        $data['name_ar'] = "السعودية";
        $data['name_en'] = "Saudia";
        $data['currency_ar'] ="ريال";
        $data['currency_en'] = "Riyal";
        $data['code_name'] ="SA";
        $data['code'] = "+966";
        \App\Country::create($data);
        ////////////////////////////////
        $city['name_ar'] = "القاهرة";
        $city['name_en'] = "cairo";
        $city['country_id'] = "1";
        $city['code'] = "+02";
        \App\City::create($city);
    }
}
