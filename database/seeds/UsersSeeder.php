<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        // admin

        //
        $item['first_name'] = "Admin";
        $item['last_name'] = "Admin";
        $item['email'] = "admin@admin.com";
        $item['type'] = "admin";
        $item['password'] = bcrypt(123456);
        \App\User::create($item);
        //
        $item['first_name'] = "Admin";
        $item['last_name'] = "Admin";
        $item['email'] = "admin@mapsut.com";
        $item['type'] = "admin";
        $item['password'] = bcrypt(123456);
        \App\User::create($item);

        // user
        $item['first_name'] = "Tarek";
        $item['last_name'] = "Admin";
        $item['email'] = "ahmed@mapsut.com";
        $item['type'] = "client";
        $item['mobile'] = "+201146568373";
        \App\User::create($item);
    }

}
