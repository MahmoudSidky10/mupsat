<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('unit_ar')->nullable();
            $table->string('unit_en')->nullable();
            $table->string('size_unit_en')->nullable();
            $table->string('size_unit_ar')->nullable();
            $table->string('image')->nullable();
            $table->string('earnest')->default("0"); // 0 = NO , 1 = YES
            $table->string('sort')->nullable();
            $table->string('priceUnit_ar')->nullable();
            $table->string('priceUnit_en')->nullable();
            $table->string('typeSizeTitle_ar')->nullable();
            $table->string('typeSizeTitle_en')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
