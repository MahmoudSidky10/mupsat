<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('price')->nullable();
            $table->string('image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('code')->nullable();
            $table->string('rate')->nullable();
            $table->string('special')->default("0"); // 0 = NO , 1 = YES
            $table->text('details_en')->nullable();
            $table->text('details_ar')->nullable();
            // dimintions
            $table->longText('size_ar')->nullable();
            $table->longText('size_en')->nullable();
            $table->integer('section_id')->unsigned()->nullable();

            $table->string('minimumCount')->default("1");
            $table->string('maximumCount')->default("1");
            $table->string('increasingBy')->default("1");
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
