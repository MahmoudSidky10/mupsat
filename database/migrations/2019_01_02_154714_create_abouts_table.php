<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('name_ar')->nullable();
            $table->longText('name_en')->nullable();
            $table->string('whatsAppNumber')->nullable();
            $table->string('lng')->nullable();
            $table->string('lat')->nullable();
            $table->string('bankAccountNumber')->nullable();
            $table->string('ibanNumber')->nullable();
            $table->text('bankImage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
